#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
# path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# path = os.path.join(path, 'Python3')

import sys
# sys.path.insert(0, path)

import Robot
import numpy
import Sofa
import Sofa.Gui
import SofaRuntime  
from matplotlib import pyplot as plt


"""
    In this example, a reduced linear model is generated for the beam robot example
    the resutls are stored in files.
"""

class StiffFlopRobot(Robot.Robot): # faster simulation, bad accuracy
    def __init__(self):
        import stiff_module as sm
        root = Sofa.Core.Node("root")
        # Sofa.Gui.GUIManager.Init("myscene", "qglviewer")
        # Sofa.Gui.GUIManager.createGUI(root, __file__)
        # Sofa.Gui.GUIManager.SetDimension(1080, 1080)

        rootNode = sm.createScene(root)
        Sofa.Simulation.init(rootNode)

        print("----")
        numberOfInputs = 1
        numberOfOutputs = 0
        listOfMo= [rootNode.RigidFrames.DOFs]
        super().__init__(rootNode, listOfMo, numberOfInputs, numberOfOutputs)
        print("----")
    def setInputValue(self, inputVector: numpy.ndarray):
        super().setInputValue(inputVector)
        print(self.rootNode.RigidFrames.stiff_flop1.Bellow31.SPC.value.value)
        self.rootNode.RigidFrames.stiff_flop1.Bellow11.SPC.value = [inputVector[0]]
        self.rootNode.RigidFrames.stiff_flop1.Bellow21.SPC.value = [inputVector[0]]
        self.rootNode.RigidFrames.stiff_flop1.Bellow31.SPC.value = [inputVector[0]]
        self.rootNode.RigidFrames.stiff_flop2.Bellow12.SPC.value = [inputVector[0]]
        self.rootNode.RigidFrames.stiff_flop2.Bellow22.SPC.value = [inputVector[0]]
        self.rootNode.RigidFrames.stiff_flop2.Bellow32.SPC.value = [inputVector[0]]
        print(self.rootNode.RigidFrames.stiff_flop1.Bellow31.SPC.value.value)
        # for i in range(6):
        #         self.rootNode.actuator.forces[i,1] = inputVector[0]


class BeamCoarseRobot(Robot.Robot): # faster simulation, bad accuracy
    def __init__(self):
        import Scenes
        rootNode = Scenes.simulate_corase_beam("CompressedRowSparseMatrixd")
        numberOfInputs = 1
        numberOfOutputs = 0
        listOfMo= rootNode.listOfMO
        super().__init__(rootNode, listOfMo, numberOfInputs, numberOfOutputs)
    def setInputValue(self, inputVector: numpy.ndarray):
        super().setInputValue(inputVector)
        for i in range(len(self.rootNode.actuator.forces)):
                self.rootNode.actuator.forces[i,1] = inputVector[0]

class BeamRobot(Robot.Robot): # A more balanced simulation of the beam
    def __init__(self):
        import Scenes
        rootNode = Scenes.simulate_beam("CompressedRowSparseMatrixd")
        numberOfInputs = 1
        numberOfOutputs = 0
        listOfMo= rootNode.listOfMO
        super().__init__(rootNode, listOfMo, numberOfInputs, numberOfOutputs)
    def setInputValue(self, inputVector: numpy.ndarray):
        super().setInputValue(inputVector)
        for i in range(len(self.rootNode.actuator.forces)):
                self.rootNode.actuator.forces[i,1] = inputVector[0]

if __name__ == "__main__":
    #robot = BeamCoarseRobot()
    robot = StiffFlopRobot()
    robot.verbose=True
    dt = 0.1 # integration time step
    delta = 1e1 # size of the disturbance for matrix estimation 
    nr = 2 # number of dof to keep (the resulting state size is 2*nr [velocities ; positions])
    N=50 # number of step used trajectory segment used for POD
    
    # trajectory generation without inputs
    traj = robot.simNSteps(N,dt,record=True) 
    u = numpy.zeros([robot.numberOfInputs,1])
    # trajectory generation with constant input
    u[0]=3
    #robot.setInputValue(u)


    traj2 = robot.simNSteps(N,dt,record=True)
   
    #plot the trajectories
    traj = numpy.append(traj,traj2,0)
    y = traj[:,1,0,-1,-1]    
    plt.plot(y)
    plt.ylim(min(y),max(y))
    plt.xlabel('steps')
    print('close the plot to continue')
    plt.show()


    # get the rest state    
    robot.reachEquilibriumPoint(1000,dt)
    x_eq = robot.getMOsState()
    trajDeltaPosition = []
    print(traj.shape)
    print(x_eq.shape)

    # computation of the new state trajectory (difference with steady state)
    for i in range(0,2*N):
        trajDeltaPosition.append(traj[i,1].flatten()-x_eq[1].flatten())
    trajDeltaPosition = numpy.array(trajDeltaPosition)
    
    # POD = DSV
    U, s, V = numpy.linalg.svd(trajDeltaPosition.transpose(), full_matrices=True)
    
    # set the transformation to apply (reduced to full) before computing the linear model
    Tr = U[:,0:nr]
    robot.setReducedToFullStateMatrix(Tr)

    # Compute the reduced linear model
    u[0]=0
    robot.setInputValue(u)
            # Initialization of the scene will be done here

    robot.reachEquilibriumPoint(1000,dt,precision=1e-5) # be sure to be very close to equilibrium

    A = robot.computeReducedStateMatrix(delta,dt)
    B = robot.computeReducedInputMatrix(delta,dt)
    
    # check matrix stability. If the max eigen value >1 => bad bad bad
    w,v =numpy.linalg.eig(A)
    vp_max = numpy.max(numpy.abs(w))
    print(f'max abs eigen of matrix A is {vp_max}')

    
    numpy.savetxt('matrixU.txt',U)
    numpy.savetxt('matrixTr.txt',Tr)
    numpy.savetxt('matrixA.txt',A)
    numpy.savetxt('matrixB.txt',B)
    numpy.savetxt('vectorXeq.txt',x_eq.flatten())


