import torch.nn as nn
import torch.nn.functional as F
import torch
import json
import numpy as np
import proxsuite
import sys
import pathlib
from math import *

########################################################################
########################## Learning Related ############################
########################################################################
class MLP(nn.Module):
    """Classical MLP.
    """
    def __init__(self, input_size, output_size, latent_size, n_hidden_layers = 1, dropout_probability = 0):
        """Initialization.

        Parameters:
        -----------
            input_size: int
                The size of the input.
            output_size: int
                The size of the output.
            latent_size: int
                The size of all the hidden layers.
            n_hidden_layers: int, default = 1
                The number of hidden layers in the MLP.
        """
        super(MLP, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.latent_size = latent_size
        self.n_hidden_layers = n_hidden_layers

        self.dropout_probability = dropout_probability
        self.USE_BATCH_NORM = False # Wether to use batch normalization or not

        self.network = nn.Sequential()

        self.network.add_module("input_layer", nn.Linear(self.input_size, self.latent_size))
        self.network.add_module("input_dropout_layer", nn.Dropout(self.dropout_probability))
        self.network.add_module("input_relu_layer", nn.ReLU())
        if self.USE_BATCH_NORM:
            self.network.add_module("input_batchnorm_layer", nn.BatchNorm1d(self.latent_size))

        # Hidden layers
        for k in range(self.n_hidden_layers):
            self.network.add_module("hidden_layer_"+str(k), nn.Linear(self.latent_size, self.latent_size))
            self.network.add_module("hidden_dropout_layer_"+str(k), nn.Dropout(self.dropout_probability))
            self.network.add_module("hidden_relu_layer_"+str(k), nn.ReLU())
            if self.USE_BATCH_NORM:
                self.network.add_module("hidden_batchnorm_layer", nn.BatchNorm1d(self.latent_size))

        # Output layer
        self.network.add_module("output_layer", nn.Linear(self.latent_size, self.output_size))

    def forward(self, x):
        """Classical forward method of nn.Module.

        Parameters:
        -----------
            x: tensor
                The input of the netwok.

        Returns:
        --------
            The output of the network.
        """
        return self.network(x)
    

def create_network():
    """Create the network from the saved files.

    """

    path = str(pathlib.Path(__file__).parent.absolute()) + "/Data"
    with open(path+"/init_data.txt", 'r') as fp:
        [W0, dfree0] = json.load(fp)
    W0, dfree0 = np.array(W0), np.array(dfree0)

    with open(path+"/network_parameters.txt", 'r') as fp:
        [latent_size, n_hidden_size, output_size, input_size] = json.load(fp)

    with open(path+"/scaling.txt", 'r') as fp:
        scaling = json.load(fp)
    scaling[0] = [np.array(s) for s in scaling[0]]
    scaling[1] = [np.array(s)  for s in scaling[1]]
    scaling[2] = [np.array(s)  for s in scaling[2]]
    scaling[3] = [np.array(s)  for s in scaling[3]]

    path_model = path + "/model.pth"

    model = MLP(input_size, output_size, latent_size, n_hidden_layers=n_hidden_size, dropout_probability=0)
    checkpoint = torch.load(path_model)
    model.load_state_dict(checkpoint['model_state_dict'])

    n_constraint = input_size - output_size

    return W0, dfree0, scaling, model, n_constraint


def create_data_std(X_0, mean_features_X, std_features_X):
    """Create scaled data to provide for training the model. Use mean and standard deviation for normalizing each feature.

    Parameters:
    -----------
        X_0: Tensor of numpy arrays
            Batch of X data
        mean_features_X: list of numpy array
            Mean values by X component evaluated on all the dataset.
        std_features_X: list of numpy array
            Standard deviation values by X component evaluated on all the dataset.

    Outputs:
    --------
        X: Tensor
            Scaled data ready for training MLP.
    """
    epsilon = 0.000000001  # For ensuring not dividing by 0

    X_W_0, X_dfree_0, X_s_ae = X_0
    size_W = X_W_0.shape[0]
    _mean_features_X = mean_features_X[0][np.triu_indices(n=size_W)]
    _std_features_X = std_features_X[0][np.triu_indices(n=size_W)]

    X_W_0 = X_W_0[np.triu_indices(n=size_W)]
    X = torch.cat([(X_W_0 - _mean_features_X) / (_std_features_X + epsilon),
                   (X_dfree_0 - mean_features_X[1]) / (std_features_X[1] + epsilon),
                   (X_s_ae - mean_features_X[2]) / (std_features_X[2] + epsilon)]).float()

    return X

########################################################################
########################## Geometry Related ############################
########################################################################
def transform_3D_to_pos(x, y, z, angx, angy, angz):
    """
    Parameters
    ----------
    x, y, z: float
        The position of the rigid
    angx, angy, angz: float
        The orientation of the rigid (euler in degrees)

    Returns
    -------
    The position of the 3 points defining the rigid

    """
    # Compute rotation matrix xyz
    c1 = np.cos(angx * np.pi / 180)
    s1 = np.sin(angx * np.pi / 180)
    c2 = np.cos(angy * np.pi / 180)
    s2 = np.sin(angy * np.pi / 180)
    c3 = np.cos(angz * np.pi / 180)
    s3 = np.sin(angz * np.pi / 180)

    R = np.array([[c2 * c3, -c2 * s3, s2],
                  [c1 * s3 + c3 * s1 * s2, c1 * c3 - s1 * s2 * s3, -c2 * s1],
                  [s1 * s3 - c1 * c3 * s2, c3 * s1 + c1 * s2 * s3, c1 * c2]])

    #Coordonate of the point
    r, ang = 4, 2 * np.pi / 3
    point_coordinate = [np.matmul(R, np.array([0, r * np.sin(i * ang + np.pi / 6), r * np.cos(i * ang + np.pi / 6)])) + np.array([x, y, z]) for i in range(3)]

    return point_coordinate[0].tolist() + point_coordinate[1].tolist() + point_coordinate[2].tolist()


########################################################################
########################### Solver Related #############################
########################################################################
def predict_W_dFree(model, W0, dfree0, s_a, scaling, n_constraint, n_act_constraint, goals_pos, nb_effector = 1):

    X = [torch.tensor(W0), torch.tensor(dfree0), torch.tensor(s_a + [0]*(nb_effector*3))]
    X = create_data_std(X, scaling[0], scaling[1])

    # Prediction
    Y = model(X)

    dfree = Y[-n_constraint:].detach().numpy()
    dfree = dfree * (scaling[3][1]) + scaling[2][1]  # Rescale dfree
    dfree_a = dfree[0:n_act_constraint]
    dfree_e = dfree[n_act_constraint:]

    if nb_effector == 1:
        effector_pos_0 = [110, 0, 0]
    else:
        effector_pos_0 = transform_3D_to_pos(110, 0, 0, 0, 0, 0) #network trained for this pos
    for i in range(len(dfree_e)):
        dfree_e[i] += effector_pos_0[i] - goals_pos[i]  # (Init_pos + Relative_Disp) - Goal_pos

    W_pred = Y[:-n_constraint].detach().numpy()
    W = np.zeros((n_constraint, n_constraint))
    W[np.triu_indices(n=n_constraint)] = W_pred
    W[np.tril_indices(n=n_constraint, k=-1)] = W.T[np.tril_indices(n=n_constraint, k=-1)]
    W = W.reshape(-1)
    W = W * (scaling[3][0].reshape(-1)) + scaling[2][0].reshape(-1)  # Rescale W
    W = W.reshape(n_constraint, n_constraint)

    Waa = W[0:n_act_constraint, 0:n_act_constraint]
    Wea = W[n_act_constraint:, 0:n_act_constraint]

    return Waa, Wea, dfree_a, dfree_e


def init_QP(n_act_constraint, nb_add_constraints = 0):
    """
    Init QP inverse problem for a given configuration

    Parameters
    ----------
    n_act_constraint: int
        Number of actuator constraints
    nb_add_constraints: int
        By default, we only consider constraints on both actuation and actuation displacement.
        Additional constraints must be initialized.

    Outputs
    ----------
    problem: QProblem
        Initialized QProblem
    """
    nb_variables = n_act_constraint
    nb_constraints = 2 * nb_variables + nb_add_constraints
    problem = proxsuite.proxqp.sparse.QP(nb_variables, 0, nb_constraints) # number of variables, number of equality constraints, number of inequality constraints
    problem.settings.initial_guess = (proxsuite.proxqp.InitialGuess.WARM_START_WITH_PREVIOUS_RESULT)
    problem.settings.max_iter = 2000

    return problem


def build_QP_system(curr_lambda_a, Waa, Wea, dfree_a, dfree_e, delta_lambda_a =1, use_epsilon=False):
    """
    Build QP inverse problem for a given configuration
    Ressource: qpoases manual at https://www.coin-or.org/qpOASES/doc/3.2/manual.pdf
               proxsuite manual at https://simple-robotics.github.io/proxsuite/md_doc_2_PROXQP_API_2_ProxQP_api.html
    ----------
    Parameters
    ----------
    curr_lambda_a: list of float 
        Current actuation force exerted on the robot.
    Waa: numpy array
        Compliance matrice projected in actuator/actuator constraint space
    Wea: numpy array
        Compliance matrice projected in effector/actuators constraint space
    dfree_a: numpy array
        Actuators displacement without any actuation
    dfree_e: numpy array
        Effectors displacement without any actuation
    delta_lambda_a: float
        Maximum actuation value enabled between two simulation time steps.
    use_epsilon : bool
        An energy term added in the minimization process.
        Epsilon has to be chosen sufficiently small so that the deformation energy does not disrupt the quality of the effector positioning.
    ----------
    Outputs
    ----------
    H: numpy array
        Quadratic cost matrix
    g: numpy array
        Linear cost matrix
    A: numpy array
        Constraint matrix
    lb: numpy array
        Lower bound for actuation
    ub: numpy array
        Upper bound for actuation
    lbA: numpy array
        Lower bound for actuation displacement
    ubA: numpy array
        Upper bound for actuation displacement
    """

    # Init QP problem with our data at dt
    if use_epsilon:
        epsilon = 0.01 * np.linalg.norm(np.dot(np.transpose(Wea), Wea), ord=1) / np.linalg.norm(Waa, ord=1)
    else:
        epsilon = 0.0

    H = np.dot(np.transpose(Wea), Wea).astype('double') + epsilon * (Waa).astype('double')
    g = np.dot(np.transpose(Wea), dfree_e).astype('double')
    A = Waa.astype('double')
    # lb = np.array([0 for i in range(len(dfree_a))]).astype(
    #     'double')  # Lower bound for actuation effort constraint - specific to both a cable and a cavity
    # ub = np.array([np.inf for i in range(len(dfree_a))]).astype(
    #     'double')  # Upper bound for actuation effort constraint - specific to both a cable a cavity
    
    lb = (np.array([ max(0, curr_lambda_a[i] - delta_lambda_a) 
                    for i in range(len(dfree_a))])).astype('double')  # Lower bound for actuation effort constraint - specific to both a cable and a cavity
    ub = (np.array([ min(np.inf, curr_lambda_a[i] + delta_lambda_a) 
                    for i in range(len(dfree_a))])).astype('double')  # Upper bound for actuation effort constraint - specific to both a cable a cavity
    
    lbA = (np.array([-np.inf for i in range(len(dfree_a))])).astype('double')
    ubA = (np.array([np.inf for i in range(len(dfree_a))])).astype('double')

    return H, g, A, lb, ub, lbA, ubA

def solve_QP(problem, H, g, A, lb, ub, lbA, ubA, is_init = True):
    """
    Solve QP inverse for a given configuration
    Ressource: qpoases manual at https://www.coin-or.org/qpOASES/doc/3.2/manual.pdf
               proxsuite manual at https://simple-robotics.github.io/proxsuite/md_doc_2_PROXQP_API_2_ProxQP_api.html
    ----------
    Parameters
    ----------
    problem: QProblem
        Initialized QProblem
    H: numpy array
        Quadratic cost matrix
    g: numpy array
        Linear cost matrix
    A: numpy array
        Constraint matrix
    lb: numpy array
        Lower bound for actuation
    ub: numpy array
        Upper bound for actuation
    lbA: numpy array
        Lower bound for actuation displacement
    ubA: numpy array
        Upper bound for actuation displacement
    is_init: bool
        True if the solver has already been initialized
    ----------
    Outputs
    ----------
    lambda_a: numpy array
        Actuation displacement
    """
    nWSR = 2000 # Number of QP iterations
    n_constraints = len(lb)


    """
    QP problem shape is:
    argmin_x  1/2 xT H x + gT(w0) x
    s. t. A(w0)x ≤ b(w0) ,
    C(w0)x ≤ u(w0)
    """

    # Displacement constraints
    C = A
    l = lbA
    u = ubA

    # Actuation effort constraints
    # Constraint lb(w0) ≤ x ≤ ub(w0) is equivalent to constraints -x ≤ -lb(w0) and x ≤ ub(w0)
    nb_variables = H.shape[0]
    I = np.eye(nb_variables)

    # Final constraint matrices
    C = np.concatenate((C, np.array(I)), axis=0)
    l = np.concatenate([l, lb])
    u = np.concatenate([u, ub])

    if is_init:
        problem.update(H, g, None, None, C, l, u)
    else:
        problem.init(H, g, None, None, C, l, u)

    problem.solve()
    lambda_a = problem.results.x

    return lambda_a

def compute_delta_a(lambda_a, Waa, dfree_a):
    return np.matmul(Waa, lambda_a) + dfree_a

def current_actuation():
    print("[ERROR] >> Recover the true value of the actuation (volumeGrowth of each cavity).")
    exit(1)

def send_to_robot(lambda_a):
    print("[ERROR] >> Apply lambda_a to your robot.")
    exit(1)

def current_effector_pos():
    print("[ERROR] >> Recover the true value of the effector position.")
    exit(1)
    



########################################################################
######################### Closed-Loop Related ##########################
########################################################################
class PID():
    def __init__(self, P, I, D):
        self.P = P
        self.I = I
        self.D = D

        self.pred_error = 0
        self.corr_integral = 0

    def reinit(self):

        self.pred_error = 0
        self.corr_integral = 0

    def compute_correction(self, real_pos, goal):

        # print("-----------")
        # print(real_pos)
        # print(goal)

        error = goal - real_pos

        corr_prop = self.P * error
        self.corr_integral = self.corr_integral + self.I * error
        corr_deriv = self.D * (error - self.pred_error)

        self.pred_error = error

        return goal + corr_prop + self.corr_integral + corr_deriv
    
    
def check_position_error(p1, p2, threshold = 0.1):
    """
    Compute the euclidean error between two positions.
    Return a boolean indicating whether the error is less than a givne threshold.
    ----------
    Parameters
    ----------
    p1: numpy array
        First position
    p2: numpy array
        Second position
    ----------
    Outputs
    ----------
    result: boolean
        Indicate if the error is below the given threshold or not
    """
    
    squared_dist = np.sum((p1-p2)**2, axis=0)
    dist = np.sqrt(squared_dist)
    if dist < threshold:
        return True
    return False
        

    
########################################################################
######################## Trajectories Related ##########################
########################################################################
def SpiralPatternGenerator(x_zone_size,y_zone_size,plan_height,point_number):
    pi = 3.1415
    pattern_tab = []
    
    r_max = max(x_zone_size,y_zone_size)
    sprial_number = ceil(r_max/0.2); # 2 mm per round
    for index_r in np.linspace(0, sprial_number*2*pi, point_number):
        r = index_r
        x=r*cos(index_r)
        y=r*sin(index_r)
        pattern_tab.append(np.array([plan_height,y,x]))

    return pattern_tab


def Spiral3D(center, radius, n_samples, steps_to_spiral, is_inverse = False):
    
    points = []

    height_increment = 10.0 / n_samples
    additional_height = 0.0
    angle_increment = 2 * 6.28319 / n_samples
    additional_angle = 0.0
    
    # Starting goal pos to first goal pos on the trajectory
    start_spiral_point = np.array([center[0] + additional_height,
                                   center[1] + radius * np.cos(additional_angle),
                                   center[2] + radius * np.sin(additional_angle)])
    for i in range(steps_to_spiral):
        x = center[0] + i * (start_spiral_point[0] - center[0]) / steps_to_spiral
        y = center[1] + i * (start_spiral_point[1] - center[1]) / steps_to_spiral
        z = center[2] + i * (start_spiral_point[2] - center[2]) / steps_to_spiral
        points.append(np.array([x,y,z]))
    
    for i in range(n_samples - steps_to_spiral):
        if is_inverse:
            x = center[0] + additional_height
        else:  
            x = center[0] - additional_height
        y = center[1] + radius * np.cos(additional_angle)
        z = center[2] + radius * np.sin(additional_angle)
        points.append(np.array([x,y,z]))

        additional_height += height_increment
        additional_angle += angle_increment
        
    return points


def Circle(center, radius, n_samples, steps_to_circle, center_start = [110, 0, 0]):
    
    points = []
    curr_angle = 0
    angle_increment =  6.28319 / n_samples
    additional_angle = 0.0
        
    # Starting goal pos to first goal pos on the trajectory
    start_circle_point = np.array([center[0],
                                   center[1] + radius * np.cos(curr_angle),
                                   center[2] + radius * np.sin(curr_angle)])
    for i in range(steps_to_circle):
        x = center_start[0] + i * (start_circle_point[0] - center_start[0]) / steps_to_circle
        y = center_start[1] + i * (start_circle_point[1] - center_start[1]) / steps_to_circle
        z = center_start[2] + i * (start_circle_point[2] - center_start[2]) / steps_to_circle
        points.append(np.array([x,y,z]))
    
    for i in range(n_samples):
        x = center[0] 
        y = center[1] + radius * np.cos(additional_angle)
        z = center[2] + radius * np.sin(additional_angle)
        points.append(np.array([x,y,z]))

        additional_angle += angle_increment
        
    return points
    
    
def Traj_To_Target(x_target, radius, angular_pos_target, n_samples, center_start = [110, 0, 0]):
   
    points = []
        
    # Starting goal pos to first goal pos on the trajectory
    start_circle_point = np.array([x_target,
                                   0 + radius * np.cos(angular_pos_target),
                                   0 + radius * np.sin(angular_pos_target)])
    for i in range(n_samples):
        x = center_start[0] + i * (start_circle_point[0] - center_start[0]) / n_samples
        y = center_start[1] + i * (start_circle_point[1] - center_start[1]) / n_samples
        z = center_start[2] + i * (start_circle_point[2] - center_start[2]) / n_samples
        points.append(np.array([x,y,z]))
    
    return points
    

def CirclePatternGenerator(r , pos_init,nb_iter,h_circle):
    pi = 3.1415
    pattern_tab = []
    nb_iter_d = 50
    h_effector = pos_init[0]
    height_shift = h_effector - h_circle
    
    d = pos_init
    for iter in range(nb_iter_d):
        y = (iter/nb_iter_d)*r
        x = h_effector - (iter/nb_iter_d)*height_shift
        pattern_tab.append([x,y,0])
    for iter in range(nb_iter):
        y = r*cos((iter/nb_iter)*2*pi);
        z = r*sin((iter/nb_iter)*2*pi);
        pattern_tab.append([x,y,z])


    return pattern_tab



