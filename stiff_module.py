"""
Auteur : Paul Chaillou
Contact : paul.chaillou@inria.fr
Année : 2022
Propriétaire : Université de Lille - CNRS 
License : Non définie, mais développé dans une démarche Open-Source et Logiciel Libre avec volonté de partage et de travail collaboratif. Développé dans un but non-marchand, en cas d'utilisation commerciale, merci de minimiser les prix et de favoriser le partage gratuit de tout ce qui peut l'être. A utiliser dans des buts prenant en compte les questions éthiques et morales (si possible non-militaire, ne rentrant pas dans le cadre de compétition, de monopole, ou de favorisation d'interets privés).
"""

 # coding=utf-8

import Sofa
# import SofaPython3
from math import sin,cos, sqrt, acos
import array
import numpy as np
import Stiff_Function as S_F
import ExternalModules as em
import polhemus_liberty.python.Polhemus_SOFA_Controller as tracking
import flopMultiController as controller
import TrajectoryController as trajectory
import CloseLoopController as CL
import phidget1002_0b.SOFA_Controller_Phidget1002_0B as actuation
import Imaging_Controller as imaging
import utils as utils # Tanguy's trajectory

from SimuController_ucl import *
# from UCL_Controller import *
# from polhemus_liberty.python.Polhemus_SOFA_Controller import *

# try : ## pour le fonctionnement classique # ATTENTION AUX ERREURS 
#     from Stiff_Function2 import Stiff_Flop
#     from flopMultiController import *
#     from SimuController_ucl import *
#     from TrajectoryController import *
#     from CloseLoopController import *
#     from polhemus_liberty.python.Polhemus_SOFA_Controller import *
#     print("************** CLASSIC ****************")
# except : # pour pouvoir lancer le script depuis un fichier externe
#     print("*************** EXCEPTION **************")
#     from ucl_collaboration.Stiff_Function import *
#     from ucl_collaboration.flopMultiController import *
#     from ucl_collaboration.SimuController_ucl import *

#from stlib3.physics.mixedmaterial import Rigidify
#from stlib3.physics.deformable import ElasticMaterialObject
#from tutorial import *
import time

############## PARAM7TRES A FIXER ####################

## FLAG ##
act_flag = 0 # set 0 for IP (Inverse Problem resolution with QP) and 1 for direct control
version = 2 # v1 d=14mm // v2 d=11.5mm // v3 d = 10mm // v4 d = 8mm but with 4 cavities
record = 1 # 0 => no record // 1 => record # Broken with material class, need to be fix
setup = 2 # 0 => no hardware connected // 1 => UCL JILAEI SETUP // 2 => INRIA DEFROST SETUP
force_field = 1 # 0 => Tetrahedron FEM force fiels // 1 => Hexahedron FEM force field (Be crafull => you have to be coherent with the mesh files)
auto_stl = 1 # 0 = > no automatic stl completion for chamber // 1 => with automatic settings
dynamic = 1 # 0 => static (first order) // 1 => dynamic
method = 0 # 0 => Full_FEM // 1 => One beam
orientation_flag = 0 # 0 => orientation is neglected / 1 => orientation is used
tracking_flag = 1 # 0 => no tracking (Polhemus or Aurora)
use_imaging = 0 # 0 => no imaging / 1 => imaging



close_loop = 0 # 0 => open loop
##     ##

if close_loop == 0 :
    K_P = 0
    K_I = 0
    shift = 0 #5 # shift in mm between the goal and the goal2 (for grabbing) points
else :    
    K_P = 0.01 #0.1
    K_I = 0.05 # 0.05
    # K_I = 0.02
    shift = 0

# synchro_flag = False
synchro_flag = imaging.Synchro_variable(synchro_flag = False)

dt = 0.1 # 0.1

print_flag = False 

if print_flag == True :
    actuation_pressure_print_flag = True 
else :
    actuation_pressure_print_flag = False 
# actuation_pressure_print_flag = True 


## Actuation Parameters ## 
pas = 10 # Pressure step for direct control (kPa) // pour argument controller (attention aux unités !) (*dt pour dyn)
max_pression = 200 # Maximal pressure in kPa
min_pression = 1
init_pressure_value = 0
# value_type = "1" # pour commande en volume (avec beam6)
value_type = "pressure" # pour commande en pression (avec beam5)
# value_type = "volumeGrowth" # pour commande en pression (avec beam5)

# rayleighMass = 0.1
# rayleighStiffness = 0.3

if dynamic == 1 :
    pas = pas*dt
    max_pression = max_pression*dt # Maximal pressure in kPa
    min_pression = min_pression*dt
    init_pressure_value = init_pressure_value*dt

## Robot Parameters ##
nb_module = 2 # nombre de modules
# module
masse_module = 0.0045 # in kg, equivalent to 4g (put 10g for the 14mm diameter one) # je met 1g, sen supposant que la répartition du poids sur les noeuds du maillage en augmente la rigidité
# soft part
coef_poisson = 0.45 # 0.15 // 0.4 # coefficient de poisson
# stiff parts
rigid_bool = 0 # 0 => no rigid parts (pas de partie rigide) // 1 => rigids parts  
YM_stiff_part = 1875 # young modulus of the stiff part
rigid_base = 4 # hauteur de rigidification de la base des modules en mm
rigid_top = 2 # hauteur de rigidification de l'extrémité des modules en mm
r_disk_chamber = 4 + 1 # +1 for the box # radius of the disk, were are put the cavities => the distance between the center of the module and the center of the cavity
r_cavity = 0.75 # radius of the cavity

if version == 1 : # V1
    h_module = 58 # hauteur du module en mm # Mieux parce que prend en compte la taille de l'effecteur ?
    chamber_model =  'stiff_flop_58_chamber_normal.stl'
    module_model = 'stiff_flop_58.vtk'
    YM_soft = 30 #30 # young modulus of the soft part (kPa)

    # h_module = 56 # hauteur du module en mm
    # chamber_model =  'model_chambres_55_simp.stl'
    # module_model = 'model_module_55.vtk'
    # YM_soft = 30 # young modulus of the soft part (kPa)
    radius = 7
    nb_cavity = 3  # nombre de cavités (ou paires de cavités)

elif version == 2 : # V2 module
    h_module = 55 # hauteur du module en mm
    if auto_stl == 0:
        # chamber_model =  'chambres_55_4it.stl'  # 55 mm
        chamber_model =  'model_chambres_v2_reg.stl' ### 
        # chamber_model =  'chambres_55cutted.stl'
        # chamber_model =  'model_chambre_regulier_cutted.stl'
    elif auto_stl == 1 :
        # stl_base = "cutted_chamber_v2_" # BE CAREFULL -  IT WILL ONLY WORK FOR SIZE BETWEEN 50 and 55mm (for the moment)
        stl_base = "bullshit" # RENDRE CA CLAIR ET PROPRE
        chamber_model = stl_base + str(h_module) + '.stl'
        # chamber_model = auto_stl_choice(h_module,stl_base)
    # module_model = 'coeur_module01.vtk'
    # module_model = 'stiff_flop_indicesOK_flip05.vtk'
    module_model = 'stiff_flop_indicesOK_flip.obj'
    # module_model = 'model_module_v2_90.vtk' # h_module = 50 du coup
    radius = 5.75 
    YM_soft = 100 # 22.5 # young modulus of the soft part (kPa)
    # YM_soft_base = 70 # 22.5 # young modulus of the soft part (kPa) # dans Material_Properties direct
    # YM_soft_top = 22.5 # 22.5 # young modulus of the soft part (kPa)
    nb_cavity = 3  # nombre de cavités (ou paires de cavités)

elif version == 3 : # V3 module
    h_module = 60 # hauteur du module en mm
    chamber_model =  'model_chambres_generic_60_simp.stl'
    # chamber_model =  'model_chambres_v2_90_test.stl'  ###  
    module_model = 'model_module_v3_canule_ext_60.vtk'
    radius = 5.75 
    YM_soft = 15 # young modulus of the soft part (kPa)
    nb_cavity = 3  # nombre de cavités (ou paires de cavités)

elif version == 4 : # V4 module
    h_module = 45 # hauteur du module en mm
    chamber_model =  'model_chambres_v4_simp.stl'
    # chamber_model =  'model_chambres_v2_90_test.stl'
    module_model = 'model_module_v4.vtk'
    radius = 5.75 
    YM_soft = 15 # young modulus of the soft part (kPa)
    nb_cavity = 4  # nombre de cavités (ou paires de cavités)

## Simulation Parameters ##
name_module = 'Module' # pas utilisé il me semble
name_cavity = 'Bellow'
#nb_poutre = nb_module*17 # (best 7 beam with 20 slices)
nb_slices = 16 #
nb_poutre_per_module = nb_slices 
# nb_poutre_per_module = 11 
nb_poutre = nb_module*nb_poutre_per_module + 1
h_effector = h_module * nb_module
goal_pas = 5 # step in mm for displacement of goal point with the keyboard
#

## Trajectory parameters ## 
# CIRCLE
circle_radius = 30
nb_iter_circle = 200 # 200 recommended  # 600 eq to 1min/tour approximately
circle_height = h_effector + 5
# SQUARE
nb_iter_square = 200 # 600 eq 10min/tour /// 
square_height = circle_height
square_radius = 15

# POINT PER POINT TRAJECTORY
# point_tab = [ [5,5,55], [10,10,55],[10,5,55],[10,0,55],[10,-5,55],[10,-10,55],[5,-10,55],[5,-5,55],[5,0,55], [0,0,55],[0,0,60], [-5,5,60], [-10,10,60],[-10,5,60],[-10,0,60],[-10,-5,60],[-10,-10,60],[-5,-10,60],[-5,-5,60],[-5,0,60], [0,0,60], [0,0,55]] # once the robot will have reach all the positions, he will start again with the 1st position
# point_tab = [ [105,5,5], [105,10,10],[110,5,10],[110,0,10],[110,-5,10],[110,-10,10],[110,-10,5],[110,-5,5],[110,0,5], [110,0,0]]#,[0,0,60], [-5,5,60], [-10,10,60],[-10,5,60],[-10,0,60],[-10,-5,60],[-10,-10,60],[-5,-10,60],[-5,-5,60],[-5,0,60], [0,0,60], [0,0,55]] # once the robot will have reach all the positions, he will start again with the 1st position
# point_tab = [ [ 77, 26, -44],[44 ,70 ,-31],[51,73,3]]

# point_tab = trajectory.PatternGenerator(x_zone_size= 30,y_zone_size=30,plan_height=h_effector+5,step=1)
# point_tab = trajectory.SpiralPatternGenerator(x_zone_size=40,y_zone_size=40,plan_height = 110,point_number=24000)
point_tab = utils.Circle(center = np.array([110, 0, 0]), radius = 40,  n_samples = 100, steps_to_circle = 50)
# point_tab = trajectory.SpiralPetoux()
# print(point_tab)
# point_tab = [ [110,0,0],[110,0,0],[110,0,0],[110,0,0],[110,0,0],[110,0,0],[110,0,0],[110,0,0],[110,10,20],[110,10,20],[110,10,20],[110,10,20],[110,10,20],[110,10,20],[110,10,20] ]
d_et_h = str(datetime.now())
nom_dossier = d_et_h[0:19]

position = [0,0,h_effector]
############## PARAM7TRES -- FIN ###################

# stiff = Stiff_Flop(h_module,init_pressure_value,value_type,YM_soft_part,YM_stiff_part,coef_poi,nb_cavity,chamber_model,nb_module,module_model,max_pression,name_cavity,masse_module,nb_poutre,rigid_base,rigid_top,rigid_bool)

def MyScene(rootNode, out_flag,step,YM_soft_part,coef_poi,act_flag,data_exp):


    # rootNode.addObject('AddPluginRepository', path = '/home/pchaillo/Documents/10-SOFA/sofa/build/master/external_directories/plugins/SoftRobots/lib/') #libSoftRobots.so 1.0
    # rootNode.addObject('AddPluginRepository', path = '/home/pchaillo/Documents/10-SOFA/sofa/build/master/external_directories/plugins/ModelOrderReduction/lib/') #libSoftRobots.so 1.0
    # rootNode.addObject('AddPluginRepository', path = '/home/pchaillo/Documents/10-SOFA/sofa/build/master/external_directories/plugins/BeamAdapter/lib')#/libBeamAdapter.so 1.0

    # required plugins:
    pluginNode  = rootNode.addChild('pluginNode')
    pluginNode.addObject('RequiredPlugin', name='SoftRobots.Inverse') # Where is SofaValidation ? => Deprecated Error in terminal
    pluginNode.addObject('RequiredPlugin', name='SoftRobots')
    pluginNode.addObject('RequiredPlugin', name='BeamAdapter')
    pluginNode.addObject('RequiredPlugin', name='SOFA.Component.IO.Mesh')
    pluginNode.addObject('RequiredPlugin', name='SOFA.Component.Engine.Generate')
    pluginNode.addObject('RequiredPlugin', name='SOFA.Component.Mass')
    pluginNode.addObject('RequiredPlugin', name='SOFA.Component.LinearSolver.Direct')
    pluginNode.addObject('RequiredPlugin', name='SOFA.Component.Constraint.Lagrangian.Correction')  
    pluginNode.addObject('RequiredPlugin', name='Sofa.GL.Component.Rendering3D') 
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.Diffusion')
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.AnimationLoop') # Needed to use components [FreeMotionAnimationLoop]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.Collision.Geometry') # Needed to use components [SphereCollisionModel]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.Constraint.Lagrangian.Correction') # Needed to use components [GenericConstraintCorrection,UncoupledConstraintCorrection]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.Constraint.Lagrangian.Solver') # Needed to use components [GenericConstraintSolver]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.Engine.Generate') # Needed to use components [ExtrudeQuadsAndGenerateHexas]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.Engine.Select') # Needed to use components [BoxROI]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.IO.Mesh') # Needed to use components [MeshOBJLoader]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.LinearSolver.Direct') # Needed to use components [SparseLDLSolver]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.LinearSolver.Iterative') # Needed to use components [CGLinearSolver]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.Mass') # Needed to use components [UniformMass]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.ODESolver.Backward') # Needed to use components [EulerImplicitSolver]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.Setting') # Needed to use components [BackgroundSetting]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.SolidMechanics.FEM.Elastic') # Needed to use components [HexahedronFEMForceField]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.SolidMechanics.Spring') # Needed to use components [RestShapeSpringsForceField]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.StateContainer') # Needed to use components [MechanicalObject]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.Topology.Container.Dynamic') # Needed to use components [HexahedronSetTopologyContainer,TriangleSetTopologyContainer]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.Topology.Container.Grid') # Needed to use components [RegularGridTopology]  
    pluginNode.addObject('RequiredPlugin', name='Sofa.Component.Visual') # Needed to use components [VisualStyle]  


    rootNode.findData('gravity').value=[9810, 0, 0];
    # rootNode.findData('gravity').value=[0, 0, 0];

        #visual dispaly
    rootNode.addObject('VisualStyle', displayFlags='showVisualModels showBehaviorModels showCollisionModels hideBoundingCollisionModels showForceFields showInteractionForceFields hideWireframe')
    rootNode.addObject('BackgroundSetting', color='0 0.168627 0.211765')

    rootNode.addObject('OglSceneFrame', style="Arrows", alignment="TopRight") # ne marche pas sans GUI
    
    rootNode.findData('dt').value= dt;
    
    rootNode.addObject('FreeMotionAnimationLoop')
    rootNode.addObject('DefaultVisualManagerLoop')      

    stiff = S_F.Stiff_Flop(h_module,init_pressure_value,value_type,nb_cavity,chamber_model,nb_module,module_model,max_pression,name_cavity,masse_module,nb_poutre,rigid_base,rigid_top,rigid_bool,min_pression,force_field,dynamic,dt,nb_slices,r_disk_chamber,r_cavity)
    material = S_F.Material_properties(YM_soft_part =YM_soft_part, YM_soft_base = 150, YM_soft_top = 90,YM_stiff_part = YM_stiff_part, coef_poi = coef_poi ,rayleighMass = 0, rayleighStiffness = 0) # faire pareil pour les dimensions # YM_base = 70

   
    if method == 0 : # pour le control direct seulement (pour l'instant)
        stiff_flop = stiff.createRobot_FullFEM(parent = rootNode, name = "MyStiffFlop",out_flag = out_flag, act_flag = act_flag,material = material) # tout remettre dans le même fichier ?
        LumenInd = stiff_flop.addObject('BoxROI', name='LumenInd', box=[-1+h_module, -2.5, -2.5, 1+h_module, 2.5, 2.5], drawBoxes=True, strict=True,drawTetrahedra = False) # si autom complète, mettre 8 dépendant des dimensions du robot
        LumenInd.init()
        BaseBox = stiff_flop.addObject('BoxROI', name='boxROI_base', box=[-1, -8, -8, 2, 8, 8], drawBoxes=False, strict=True,drawTetrahedra = False) # si autom complète, mettre 8 dépendant des dimensions du robot
        BaseBox.init()
        stiff_flop.addObject('RestShapeSpringsForceField', points=BaseBox.indices.value, angularStiffness=1e5, stiffness=1e5) # pour accrocher la base du robot dans l'espace
        noeud_solver = stiff_flop

    elif method == 1 :
        rigidFramesNode  = rootNode.addChild('RigidFrames')
        rigidFramesNode.addObject('RegularGridTopology',  name='meshLinesCombined',  nx=nb_poutre, ny='1', nz='1', xmax=h_module*nb_module, xmin='0.0', ymin='0', ymax='0',zmin='0',zmax='0')
        # rigidFramesNode.addObject('RegularGridTopology',  name='meshLinesCombined',  nx='1', ny='1', nz=nb_poutre, xmax='0.0', xmin='0.0', ymin='0', ymax='0',zmin='0',zmax=h_module*nb_module)
        rigidFramesNode.addObject('MechanicalObject',  name='DOFs', template='Rigid3d', showObject='1', showObjectScale='1', rotation=[0, -0 ,0], translation = [0,0,0]) # -90 on y
        rigidFramesNode.addObject('BeamInterpolation', name='BeamInterpolation', printLog = '1', dofsAndBeamsAligned='true', straight='1', crossSectionShape='circular', radius=radius)#, radius=5*radius)
        # rigidFramesNode.addObject('AdaptiveBeamForceFieldAndMass', name='BeamForceField', computeMass='0', massDensity=0.001)
        rigidFramesNode.addObject('RestShapeSpringsForceField', name='anchor', points='0', stiffness='1e12', angularStiffness='1e12') # pour accrocher la base du robot dans l'espace
        stiff_flop = stiff.createRobot(parent = rigidFramesNode, name = "MyStiffFlop",out_flag = out_flag, act_flag = act_flag,material = material,print_flag = False)
        noeud_solver = rigidFramesNode

    ## Classic
    noeud_solver.addObject('SparseLDLSolver', name='ldlsolveur',template="CompressedRowSparseMatrixMat3x3d")
    noeud_solver.addObject('GenericConstraintCorrection')

    ## Asyncrhone
    # noeud_solver.addObject('ShewchukPCGLinearSolver', name='PCG',preconditioner = "@preconditioner",)
    # noeud_solver.addObject('AsyncSparseLDLSolver', name='preconditioner',template="CompressedRowSparseMatrixMat3x3d")
    # noeud_solver.addObject('GenericConstraintCorrection',linearSolver = "@preconditioner")

    ## Tests
    # parent.addObject('MatrixLinearSystem', template="CompressedRowSparseMatrixd")
    # noeud_solver.addObject('EigenSimplicialLDLT', name='eigen_solveur',template="CompressedRowSparseMatrixMat3x3d",ordering = "Natural")
    # noeud_solver.addObject('CGLinearSolver', name='ldlsolveur',template="GraphScattered")    
    # parent.addObject('GlobalSystemMatrixExporter', exportEveryNumberOfSteps = 1, filename="matrix", format = 'csv')

    
    if dynamic == 1 : # à quel niveau est-il préférable de mettre ce solver ?
        noeud_solver.addObject('EulerImplicitSolver', firstOrder='0', vdamping=0,rayleighStiffness='0',rayleighMass='0') # jouer avec ça pour capturer la dynamique => tout mettre à 0 ici et choisir les valeurs dans les matériaux (ForceField)
    else :
        noeud_solver.addObject('EulerImplicitSolver', firstOrder='1', vdamping=0)

    if nb_module == 2 :
        stiff_2 = noeud_solver.getChild('stiff_flop1')

    # if nb_module == 2 : # goal point pour la position intermédiaire
    #     MeasuredPosition_2 = EffectorGoal(node=rootNode, position = [0,0,h_effector/2],name = 'MeasuredPosition_2',taille = 7)

    if act_flag == 0 :
        rootNode.addObject('QPInverseProblemSolver', name="QP", printLog='0', saveMatrices = True ,epsilon = 0.01) # initialement epsilon = 0.001

         ## 2eme goal poitn pour décalage, permet de mieux attraper à la souris
        if close_loop == 0 :
            goal2 = S_F.EffectorGoal(node=rootNode, position = [h_effector+shift,0,0],name = 'goal2',taille = 2)
            # goal2 = S_F.EffectorGoal(node=rootNode, position = [h_effector+shift,10,20],name = 'goal2',taille = 2)

        if orientation_flag == 0 :
            # MeasuredPosition = S_F.EffectorGoal(node=rootNode, position = [0,0,0],name = 'MeasuredPosition',taille = 4) # CLASSIC 
            DesiredPosition = S_F.EffectorGoal(node=rootNode, position = [h_effector+5,20,20],name = 'DesiredPosition',taille = 1)
            # CLASSIC 
            goal = S_F.EffectorGoal(node=rootNode, position = [h_effector,0,0],name = 'goal',taille = 0.5)
            controlledPoints = stiff_flop.addChild('controlledPoints')
            controlledPoints.addObject('MechanicalObject', name="actuatedPoints", template="Vec3",position=[h_effector, 0, 0])#,rotation=[0, 90 ,0]) # classic
            # controlledPoints.addObject('MechanicalObject', name="actuatedPoints", template="Rigid3",position=[0, 0, h_effector,0., 0., 0., 1.])#,rotation=[0, 90 ,0]) # rigid pour l'orientation
            controlledPoints.addObject('PositionEffector', template="Vec3d", indices='0', effectorGoal="@../../../goal/goalM0.position") # classic
            controlledPoints.addObject('BarycentricMapping', mapForces=False, mapMasses=False)
        else :
            DesiredPosition = S_F.EffectorGoal_Orientation(node=rootNode, position = [h_effector,0,0,0,0,0,1],name = 'DesiredPosition',taille = 3)
            # MeasuredPosition = S_F.EffectorGoal_Orientation(node=rootNode, position = [h_effector,0,0,0,0,0,1],name = 'MeasuredPosition',taille = 2)
            ## GOAL ON BEAM 
            goal = rootNode.addChild('goal')
            # goal.addObject('EulerImplicitSolver', firstOrder=True)
            # goal.addObject('CGLinearSolver', iterations=100, threshold=1e-12, tolerance=1e-10)
            goal.addObject('MechanicalObject', name='goalM0', position=[h_effector,0,0,0,0,0,1],template = "Rigid3d",showObject = False,showObjectScale = 10)
            # goal.addObject('SphereCollisionModel', radius=0.5)
            # goal.addObject('RestShapeSpringsForceField', points=0, angularStiffness=1e5, stiffness=1e5)
            goal.addObject('UncoupledConstraintCorrection')
            controlledPoints = rigidFramesNode.addChild('controlledPoints')
            controlledPoints.addObject('MechanicalObject', name="actuatedPoints", template="Rigid3d",position=[0, 0, 0,0,0,0,1],showObject = False,showObjectScale = 5)#,rotation=[0, 90 ,0]) # classic
            controlledPoints.addObject('PositionEffector', template="Rigid3d", indices='0', effectorGoal="@../../goal/goalM0.position",useDirections=[1, 1, 1, 0, 0, 0],weight = 1) # classic
            # controlledPoints.addObject('BarycentricMapping', mapForces=False, mapMasses=False)
            # controlledPoints.addObject('IdentityMapping', mapForces=False, mapMasses=False)
            controlledPoints.addObject("RigidRigidMapping", index=nb_poutre-1) 
            controlledPoints_orientation = rigidFramesNode.addChild('controlledPoints_orientation')
            controlledPoints_orientation.addObject('MechanicalObject', name="actuatedPoints", template="Rigid3d",position=[0, 0, 0,0,0,0,1],showObject = False,showObjectScale = 5)#,rotation=[0, 90 ,0]) # classic
            controlledPoints_orientation.addObject('PositionEffector', template="Rigid3d", indices='0', effectorGoal="@../../goal/goalM0.position",useDirections=[0, 0, 0, 1, 1, 1],weight = 10) # classic
            # controlledPoints.addObject('BarycentricMapping', mapForces=False, mapMasses=False)
            # controlledPoints.addObject('IdentityMapping', mapForces=False, mapMasses=False)
            controlledPoints_orientation.addObject("RigidRigidMapping", index=nb_poutre-1) 

    elif act_flag == 1:
        rootNode.addObject('GenericConstraintSolver', maxIterations='100', tolerance = '0.0000001')


    if tracking_flag == 1 :
            MeasuredPosition = S_F.EffectorGoal_Orientation(node=rootNode, position = [h_effector,0,0,0,0,0,1],name = 'MeasuredPosition',taille = 0.5,solver=False)



    if out_flag == 1: # CONTROLLER SIMU AUTOMATIQUE
        if data_exp[0] == "lonely" : # Dans le cas ou on ne teste qu'une valeur
            cam_pos_tab = data_exp[4]
            rootNode.addObject("InteractiveCamera", name="camera", position=cam_pos_tab)#, zNear=0.1, zFar=500, computeZClip = False,  projectionType=0)
            rootNode.addObject(simu_controller(name="simu_controller",step = 0,RootNode=rootNode,data_exp=data_exp))#, Step=step))
            rootNode.addObject(StaticPressure(name="simu_static_pressure",module =stiff,RootNode=rootNode,data_exp=data_exp))#, Step=step))
            rootNode.addObject(Static_3d_Comparator(name="simu_pos_compar",module = stiff,RootNode = rootNode,data_exp = data_exp))#, Step=step)) # ne fonctionne pas pour inverse
        else :
            # print("fete à la saucisse j'invite personne")
            cam_pos_tab = data_exp[7]
            rootNode.addObject("InteractiveCamera", name="camera", position=cam_pos_tab)#, zNear=0.1, zFar=500, computeZClip = False,  projectionType=0)
            rootNode.addObject(simu_controller(name="simu_controller",step = step,RootNode=rootNode,data_exp=data_exp))#, Step=step))
            # rootNode.addObject(SimuPrinterCsv(stiff,rootNode))
            rootNode.addObject(PositionComparator_2d(name="simu_pos_compar", step=step,module = stiff,RootNode = rootNode,data_exp = data_exp))#, Step=step)) # ne fonctionne pas pour inverse
            # rootNode.addObject(PressureComparator(name="simu_pres_compar", step=step,module = stiff,RootNode = rootNode))#, Step=step))
            # rootNode.addObject(PositionComparator_3d(name="simu_pos_compar", step=step,module = stiff,RootNode = rootNode,data_exp=data_exp))#, Step=step)) # ne fonctionne pas pour inverse
            # rootNode.addObject(PressurePrinter(name="printer", step=step,module = stiff,RootNode = rootNode))#, Step=step)) # ne fonctionne pas pour inverse
            if act_flag == 0 :
                rootNode.addObject(InversePositionController(name="inverse_csv_controller", nb_module = nb_module,nb_cavity = nb_cavity,step=step,RootNode=rootNode,data_exp=data_exp))
            elif act_flag == 1 :
                # rootNode.addObject(CsvPressureController(name="simu_csv_controller", nb_module = nb_module,nb_cavity = nb_cavity,step=step,RootNode=rootNode))#, Step=step))
                rootNode.addObject(CsvPressureController(name="simu_csv_controller",module =stiff,step=step,RootNode=rootNode,data_exp=data_exp))#, Step=step))
                # rootNode.addObject(ProgressivePressure(name="simu_pos_controller", nb_module = nb_module,nb_cavity = nb_cavity,step=step,RootNode=rootNode))#, Step=step))

    else : # controller runSofa
        if record == 1:
            if method == 0 :
                indices_tab = [2012,1649,1253,2342,1154,2738,1088,2837,1814,2573,2177,1979] # pour récupérer la position de l'effecteur pour 2 modules => Faire un truc propre avec une BoxROI
                rootNode.addObject( controller.PositionViewerFEM( node = stiff_flop, indices = indices_tab ) )
                rootNode.addObject( controller.PositionPrinterFEM( node = stiff_flop, indices = indices_tab,nom_dossier = nom_dossier,nom_fichier = 'FEM_Positions') )

            else :
                if act_flag == 0 : # On peut par la suite ajouter un else pour enregistrer aussi des information en commande directe si nécessaire
                    rootNode.addObject(controller.ParameterPrinterCsv(module =stiff,nom_dossier = nom_dossier,RootNode=rootNode,K_I = K_I, K_P = K_P,dt=dt,material = material))
                    rootNode.addObject(controller.PositionPrinterCsv(node = rigidFramesNode,name = 'DOFs',module =stiff,nom_dossier = nom_dossier,nom_fichier = 'RigidFrames',beam_ind = nb_poutre-1))
                    rootNode.addObject(controller.PositionPrinterCsv(node = goal,name = 'goalM0',module =stiff,nom_dossier = nom_dossier,nom_fichier = 'goal'))
                    rootNode.addObject(controller.PositionPrinterCsv(node = MeasuredPosition,name = 'MeasuredPositionM0',module =stiff,nom_dossier = nom_dossier,nom_fichier = 'MeasuredPosition'))
                    if close_loop == 1 :
                        rootNode.addObject(controller.PositionPrinterCsv(node = DesiredPosition,name = 'DesiredPositionM0',module =stiff,nom_dossier = nom_dossier,nom_fichier='Desiredposition'))
                    else :
                        rootNode.addObject(controller.PositionPrinterCsv(node = goal2,name = 'goal2M0',module =stiff,nom_dossier = nom_dossier,nom_fichier = 'goal2'))
                    if nb_module == 2 :
                        # rootNode.addObject(controller.PositionPrinterCsv(node = MeasuredPosition_2,name = 'MeasuredPosition_2M0',module =stiff,nom_dossier = nom_dossier,nom_fichier = 'MeasuredPosition2')) # line 364 and 365 to record intermediairy (middle of robot) position
                        # rootNode.addObject(controller.PositionPrinterCsv(node = rigidFramesNode,name = 'DOFs',module =stiff,nom_dossier = nom_dossier,nom_fichier = 'RigidFramesMiddle',beam_ind = (nb_poutre/2)-1))
                        rootNode.addObject(controller.PressurePrinterCsv(module =stiff,nom_dossier = nom_dossier,parent=stiff_flop,act_flag=act_flag,node2 = stiff_2))
                    else :
                        rootNode.addObject(controller.PressurePrinterCsv(module =stiff,nom_dossier = nom_dossier,parent=stiff_flop,act_flag=act_flag))
                elif act_flag == 1 :
                    rootNode.addObject(controller.ParameterPrinterCsv(module =stiff,nom_dossier = nom_dossier,RootNode=rootNode,K_I = K_I, K_P = K_P,dt=dt,material = material))
                    rootNode.addObject(controller.PositionPrinterCsv(node = rigidFramesNode,name = 'DOFs',module =stiff,nom_dossier = nom_dossier,nom_fichier = 'RigidFrames',beam_ind = nb_poutre-1))
                    rootNode.addObject(controller.PressurePrinterCsv(module =stiff,nom_dossier = nom_dossier,parent=stiff_flop,act_flag=act_flag,node2 = stiff_2))



        if setup == 1:
            rootNode.addObject(ArduinoPressure_UCL(module = stiff,RootNode = rootNode, dt = dt)) # for UCL setup
            if nb_module == 1:
                rootNode.addObject(AuroraTracking(child_name = 'MeasuredPosition',name = 'MeasuredPositionM0',module =stiff,RootNode=rootNode))
            elif nb_module == 2:
                rootNode.addObject(AuroraTracking_2_nodes(node = MeasuredPosition,name = 'MeasuredPositionM0',node2 = MeasuredPosition_2,name2 = 'MeasuredPosition_2M0',module =stiff))
        elif setup == 2:
            # rootNode.addObject(tracking.PolhemusTracking_Rigid3(node = MeasuredPosition,name = 'MeasuredPositionM0',offset = [h_effector,0,0],axis = [2,-1,0],print_flag = True) ) # mettre un orientation_flag au début du code ?
            if nb_module == 1:
                rootNode.addObject(actuation.Phidget_pressure_actuation(module = stiff,node = stiff_flop, serial_nb = 589473)) # pour envoyer les pressions calculées par le modèle inverse au robot (hardware) # (mettre après le if suivant !)
            elif nb_module == 2:
                if method == 1 :
                    rootNode.addObject(actuation.Phidget_pressure_actuation(module = stiff,node = stiff_2, serial_nb = 589531,print_flag =actuation_pressure_print_flag)) # module 1 # 589531
                    rootNode.addObject(actuation.Phidget_pressure_actuation(module = stiff,node = stiff_flop, serial_nb = 589473,i=1,print_flag = actuation_pressure_print_flag)) # module 2 # 589473
                else :
                    rootNode.addObject(actuation.Phidget_pressure_actuation(module = stiff,node = stiff_flop, serial_nb = 589531,print_flag =actuation_pressure_print_flag,i=0)) # module 1 # 589531
                    rootNode.addObject(actuation.Phidget_pressure_actuation(module = stiff,node = stiff_flop, serial_nb = 589473,i=1,print_flag = actuation_pressure_print_flag)) # module 2 # 589473

        if use_imaging == 1 :
            rootNode.addObject(imaging.DirectSensorValue(nom_dossier = nom_dossier,nom_fichier = 'SensorDirectValue'))
            rootNode.addObject(imaging.Topographic_Acquisition(node = MeasuredPosition,name = 'MeasuredPositionM0',module =stiff,nom_dossier = nom_dossier,nom_fichier = '3D_acquisition_tracking'))
            rootNode.addObject(imaging.Topographic_Acquisition_with_orientation(node = MeasuredPosition,name = 'MeasuredPositionM0',module =stiff,nom_dossier = nom_dossier,nom_fichier = '3D_acquisition_tracking_with_orientation'))
            rootNode.addObject(imaging.Topographic_Acquisition_with_orientation_Synchro(node = MeasuredPosition,name = 'MeasuredPositionM0',module =stiff,nom_dossier = nom_dossier,nom_fichier = '3D_acquisition_tracking_with_orientation_synchro',synchro_flag = synchro_flag))
            rootNode.addObject(imaging.Topographic_Acquisition_with_orientation_matrices_Synchro(node = MeasuredPosition,name = 'MeasuredPositionM0',module =stiff,nom_dossier = nom_dossier,nom_fichier = '3D_acquisition_tracking_with_orientation_synchro_matrices',synchro_flag = synchro_flag))


        if act_flag == 0 :
            if close_loop == 1 :

                rootNode.addObject(controller.GoalKeyboardController(goal_pas = goal_pas,node = DesiredPosition,name = 'DesiredPositionM0')) # for goal without shift
                # rootNode.addObject(controller.GoalShift(node_follow= goal ,object_follow = 'goalM0',node_master = goal2,object_master = 'goal2M0',shift_tab = [shift,0,0]))

                if orientation_flag == 0 :
                    rootNode.addObject(CL.CloseLoopController2(name="CloseLoopController",RootNode=rootNode, K_P = K_P, K_I = K_I,print_flag = True))
                else :
                    rootNode.addObject(CL.CloseLoopController_Orientation(node_tracking = MeasuredPosition,name_tracking = "MeasuredPositionM0",node_desired = DesiredPosition ,name_desired = "DesiredPositionM0",node_command = goal ,name_command = "goalM0", K_P = K_P, K_I = K_I,print_flag = False))

                # rootNode.addObject(trajectory.PointPerPointTrajectory_Synchro(node = DesiredPosition,name = 'DesiredPositionM0',module = stiff,point_tab = point_tab, node_pos = MeasuredPosition, name_pos = 'MeasuredPositionM0',err_d = 10,shift=0,beam_flag = 0,synchro_flag = synchro_flag,print_flag = False))
                # rootNode.addObject(trajectory.PointPerPointTrajectory(node = DesiredPosition,name = 'DesiredPositionM0',module = stiff,point_tab = point_tab, node_pos = MeasuredPosition, name_pos = 'MeasuredPositionM0',err_d = 0.5,shift=0,beam_flag = 0))
                rootNode.addObject(trajectory.CircleTrajectory(rayon =circle_radius, nb_iter = nb_iter_circle,node = DesiredPosition,name = 'DesiredPositionM0',circle_height = circle_height, module=stiff,axis = [0,1,1]))
                # rootNode.addObject(PrintGoalPos(name="CloseLoopController",RootNode=rootNode))
                # rootNode.addObject(trajectory.PointPerPointTrajectory(node = DesiredPosition,name = 'DesiredPositionM0',module = stiff,point_tab = point_tab, node_pos = MeasuredPosition, name_pos = 'MeasuredPositionM0',err_d = 50,shift=0,beam_flag = 0))   

            else : # open loop ( close_loop == 0 )     
                # rootNode.addObject(PressurePrinter_local(module = stiff,node = rigidFramesNode))
                # rootNode.addObject(PositionViewer(module = stiff,node = rigidFramesNode))

                # rootNode.addObject(PointPerPointTrajectory(node = goal,name = 'goalM0',module = stiff,point_tab = point_tab, node_pos = rigidFramesNode, name_pos = 'DOFs',err_d = 50,shift=0,type_flag = 1))

                # rootNode.addObject(OrientationControl(node = goal,name = 'goalM0'))

        
                # rootNode.addObject(trajectory.LineTrajectory(nb_iter=50,node = goal2,name = 'goal2M0',p_begin = [110,0,0], p_end = [110,10,20]))
                # rootNode.addObject(trajectory.PointPerPointTrajectory(node = goal2,name = 'goal2M0',module = stiff,point_tab = point_tab, node_pos = rigidFramesNode, name_pos = 'DOFs',err_d = 50,shift=0,type_flag = 1,check_flag = True))
                # rootNode.addObject(trajectory.PointPerPointTrajectory_Synchro(node = goal2,name = 'goal2M0',module = stiff,point_tab = point_tab, node_pos = rigidFramesNode, name_pos = 'DOFs',err_d = 50,shift=0,type_flag = 1,check_flag = True,synchro_flag = 0))
                rootNode.addObject(trajectory.CircleTrajectory(rayon =circle_radius, nb_iter = nb_iter_circle, node = goal2,name = 'goal2M0',circle_height = circle_height,module=stiff,axis = [0,1,1]))
                # rootNode.addObject(trajectory.CircleTrajectory(rayon =circle_radius, nb_iter = nb_iter_circle, node = goal2,name = 'goal2M0',circle_height = circle_height,module=stiff,axis = [1,1,0]))

                # rootNode.addObject(SquareTrajectory(length =square_radius, nb_iter = nb_iter_square,node = goal2,name = 'goal2M0',square_height = square_height+0,module=stiff))

                rootNode.addObject(controller.GoalKeyboardController(goal_pas = goal_pas,node = goal2,name = 'goal2M0')) # for goal with shift
                rootNode.addObject(controller.GoalShift(node_follow= goal ,object_follow = 'goalM0',node_master = goal2,object_master = 'goal2M0',shift_tab = [shift,0,0]))

        elif act_flag == 1 :
            # rootNode.addObject(PositionViewer(module = stiff,node = rigidFramesNode,nb_poutre=nb_poutre))
            if nb_module == 1 :
                rootNode.addObject(controller.StiffController(pas=pas,module = stiff,parent = stiff_flop))
            elif nb_module == 2 :
                if method == 0 : # si full fem, alors même parent
                    rootNode.addObject(controller.StiffController(pas=pas,module = stiff,parent = stiff_flop))#,node2 = stiff_2))
                    # rootNode.addObject(controller.FixPressureScenario(pas=pas,module = stiff,parent = stiff_flop))

                else :
                    # rootNode.addObject(controller.StiffController(pas=pas,module = stiff,parent = stiff_flop,node2 = stiff_2))
                    # rootNode.addObject(controller.FixPressure(pas=pas,module = stiff,parent = stiff_flop,node2 = stiff_2))
                    rootNode.addObject(controller.FixPressureScenario(pas=pas,module = stiff,parent = stiff_flop,node2 = stiff_2))

                    # FixPressure

    print("Le code python a été executé jusqu' à la fin de la description de la scène")

    return rootNode

def createScene(rootNode):
    rootNode = MyScene(rootNode,out_flag = 0,step = 0,YM_soft_part=YM_soft,coef_poi = coef_poisson,data_exp = 0,act_flag = act_flag) # act_flag = 0- IP ; 1- Direct Control
    return rootNode
