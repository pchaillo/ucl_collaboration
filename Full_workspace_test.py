"""
Auteur : Paul Chaillou
Contact : paul.chaillou@inria.fr
Année : 2023
Propriétaire : Université de Lille - CNRS 
License : Non définie, mais développé dans une démarche Open-Source et Logiciel Libre avec volonté de partage et de travail collaboratif. Développé dans un but non-marchand, en cas d'utilisation commerciale, merci de minimiser les prix et de favoriser le partage gratuit de tout ce qui peut l'être. A utiliser dans des buts prenant en compte les questions éthiques et morales (si possible non-militaire, ne rentrant pas dans le cadre de compétition, de monopole, ou de favorisation d'interets privés).

589531 - #A
589473 - #B
"""

# from Phidget1002_0B import *

import phidget1002_0b.Phidget1002_0B as actuation
import polhemus_liberty.python.PolhemusUSB as tracking
import time as time
import Connexion_Function_ucl as connect

p = tracking.PolhemusUSB() # only once

nom_fichier = "record_02"

nf, f = connect.OpenPrintFile2(file_type = ".csv",nom_fichier=nom_fichier,nom_dossier="workspace_record")

## PARAMETERS
minimal_pressure = 0
maximal_pressure = 160 # kPa
step = 75
waiting_time = 5

channel_1 = actuation.PhidgetOutput(serial_nb = 589473,channel = 1) # 589531
channel_2 = actuation.PhidgetOutput(serial_nb = 589473,channel = 2)
channel_3 = actuation.PhidgetOutput(serial_nb = 589473,channel = 3)
channel_4 = actuation.PhidgetOutput(serial_nb = 589531,channel = 1)
channel_5 = actuation.PhidgetOutput(serial_nb = 589531,channel = 2)
channel_6 = actuation.PhidgetOutput(serial_nb = 589531,channel = 3)

# # channel.apply_voltage(1)
# channel.apply_pressure(1)

# try:
#     input("Press Enter to Start full workspace test\n")
# except (Exception, KeyboardInterrupt):
#     pass


for pres_6 in range(minimal_pressure, maximal_pressure, step):
    for pres_5 in range(minimal_pressure, maximal_pressure, step):
        for pres_4 in range(minimal_pressure, maximal_pressure, step):
            for pres_3 in range(minimal_pressure, maximal_pressure, step):
                for pres_2 in range(minimal_pressure, maximal_pressure, step):
                    for pres_1 in range(minimal_pressure, maximal_pressure, step):
                        pressure = [pres_1,pres_2,pres_3,pres_4,pres_5,pres_6]
                        print([pres_1,pres_2,pres_3,pres_4,pres_5,pres_6])
                        channel_1.apply_pressure(pres_1/100)
                        channel_2.apply_pressure(pres_2/100)
                        channel_3.apply_pressure(pres_3/100)
                        channel_4.apply_pressure(pres_4/100)
                        channel_5.apply_pressure(pres_5/100)
                        channel_6.apply_pressure(pres_6/100)
                        time.sleep(waiting_time)
                        p.UpdateSensors()
                        position = p.sensors[0].GetLastPosition()
                        orientation = p.sensors[0].GetLastQuaternion()
                        print('Position  : '+str(position)+ str(orientation)+str(pressure) )
                        f.write(str(position)+str(orientation)+str(pressure)+'\n')
                        f.close()
                        f = open(nf,'a')
                        print("test")



channel.close_channel()