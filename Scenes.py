#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import Sofa
import SofaRuntime

def simulate_beam(linear_solver_template):
    root = Sofa.Core.Node("rootNode")
    root.listOfMO = list()

    SofaRuntime.importPlugin("SofaMatrix")
    loop = root.addObject('DefaultAnimationLoop')

    root.addObject('RequiredPlugin', name='Sofa.Component.ODESolver.Backward')
    root.addObject('RequiredPlugin', name='Sofa.Component.LinearSolver.Direct')
    root.addObject('RequiredPlugin', name='Sofa.Component.Engine.Select')
    root.addObject('RequiredPlugin', name='Sofa.Component.Constraint.Projective')
    root.addObject('RequiredPlugin', name='Sofa.Component.SolidMechanics.FEM.Elastic')
    root.addObject('RequiredPlugin', name='Sofa.Component.Mass')
    root.addObject('RequiredPlugin', name='Sofa.Component.MechanicalLoad')

    root.addObject('EulerImplicitSolver', rayleighStiffness="0.1", rayleighMass="0.1")
    linear_solver = root.addObject('SparseLDLSolver', name='linear_solver',applyPermutation="false", template=linear_solver_template)
    
    MO = root.addObject('MechanicalObject', name="DoFs")
    root.listOfMO.append(MO)
    root.addObject('UniformMass', name="mass", totalMass="320")
    root.addObject('RegularGridTopology', name="grid", nx="4", ny="4", nz="20",
                   xmin="-9", xmax="-6", ymin="0", ymax="3", zmin="0", zmax="19")
    root.addObject('BoxROI', name="box", box=[-10, -1, -0.0001, -5, 4, 0.0001])
    root.addObject('FixedConstraint', indices="@box.indices")
    root.addObject('HexahedronFEMForceField', name="FEM",
                   youngModulus="4000", poissonRatio="0.3", method="large")
    boxEnd = root.addObject('BoxROI', name="boxEnd", box=[-10, -1, 19-0.0001, -5, 4, 19+0.0001])
    
    
    root.actuator = root.addObject('ConstantForceField',name = "Force", indices="@boxEnd.indices")

    # root.addObject('GlobalSystemMatrixExporter',name = 'MatrixExporter')
    Sofa.Simulation.init(root)
    #Sofa.Simulation.animate(root, 0.0001)
    forces = [[0,0,0]]*len(boxEnd.indices)
    #print(forces)
    
    root.actuator.forces = forces
    
    return root


def simulate_corase_beam(linear_solver_template):
    root = Sofa.Core.Node("rootNode")
    root.listOfMO = list()

    SofaRuntime.importPlugin("SofaMatrix")
    loop = root.addObject('DefaultAnimationLoop')

    root.addObject('RequiredPlugin', name='Sofa.Component.ODESolver.Backward')
    root.addObject('RequiredPlugin', name='Sofa.Component.LinearSolver.Direct')
    root.addObject('RequiredPlugin', name='Sofa.Component.Engine.Select')
    root.addObject('RequiredPlugin', name='Sofa.Component.Constraint.Projective')
    root.addObject('RequiredPlugin', name='Sofa.Component.SolidMechanics.FEM.Elastic')
    root.addObject('RequiredPlugin', name='Sofa.Component.Mass')
    root.addObject('RequiredPlugin', name='Sofa.Component.MechanicalLoad')

    root.addObject('EulerImplicitSolver',
                   rayleighStiffness="0.1", rayleighMass="0.1")
    linear_solver = root.addObject('SparseLDLSolver', name='linear_solver',
                                   applyPermutation="false", template=linear_solver_template)
    
    MO = root.addObject('MechanicalObject', name="DoFs")
    root.listOfMO.append(MO)
    root.addObject('UniformMass', name="mass", totalMass="320")
    root.addObject('RegularGridTopology', name="grid", nx="2", ny="2", nz="2",
                   xmin="-9", xmax="-6", ymin="0", ymax="3", zmin="0", zmax="19")
    root.addObject('BoxROI', name="box", box=[-10, -1, -0.0001, -5, 4, 0.0001])
    
    root.addObject('BoxROI', name="boxEnd", box=[-10, -1, 19-0.0001, -5, 4, 19+0.0001])
    root.addObject('FixedConstraint', indices="@box.indices")
    root.addObject('HexahedronFEMForceField', name="FEM",
                   youngModulus="4000", poissonRatio="0.3", method="large")
    
    root.actuator = root.addObject('ConstantForceField',name = "Force", indices="@boxEnd.indices",forces = [[0,0,0]]*4)

    # root.addObject('GlobalSystemMatrixExporter',name = 'MatrixExporter')
    Sofa.Simulation.init(root)
    #Sofa.Simulation.animate(root, 0.0001)

    return root
def simulate_corase_beam_explicit(linear_solver_template):
    root = Sofa.Core.Node("rootNode")
    root.listOfMO = list()

    SofaRuntime.importPlugin("SofaMatrix")
    loop = root.addObject('DefaultAnimationLoop')

    root.addObject('RequiredPlugin', name='Sofa.Component.ODESolver.Forward')
    root.addObject('RequiredPlugin', name='Sofa.Component.LinearSolver.Direct')
    root.addObject('RequiredPlugin', name='Sofa.Component.Engine.Select')
    root.addObject('RequiredPlugin',
                   name='Sofa.Component.Constraint.Projective')
    root.addObject('RequiredPlugin',
                   name='Sofa.Component.SolidMechanics.FEM.Elastic')
    root.addObject('RequiredPlugin', name='Sofa.Component.Mass')

    root.addObject('EulerExplicitSolver')
    linear_solver = root.addObject('SparseLDLSolver', name='linear_solver',
                                   applyPermutation="false", template=linear_solver_template)
    
    MO = root.addObject('MechanicalObject', name="DoFs")
    root.listOfMO.append(MO)
    root.addObject('UniformMass', name="mass", totalMass="320")
    root.addObject('RegularGridTopology', name="grid", nx="2", ny="2", nz="2",
                   xmin="-9", xmax="-6", ymin="0", ymax="3", zmin="0", zmax="19")
    root.addObject('BoxROI', name="box", box=[-10, -1, -0.0001, -5, 4, 0.0001])
    root.addObject('FixedConstraint', indices="@box.indices")
    root.addObject('HexahedronFEMForceField', name="FEM",
                   youngModulus="4000", poissonRatio="0.3", method="large",rayleighStiffness = '200')
    # root.addObject('GlobalSystemMatrixExporter',name = 'MatrixExporter')
    Sofa.Simulation.init(root)
    #Sofa.Simulation.animate(root, 0.0001)

    return root
def simulate_two_beams(linear_solver_template):
    root = Sofa.Core.Node("rootNode")
    root.listOfMO = list()
    
    SofaRuntime.importPlugin("SofaMatrix")
    loop = root.addObject('DefaultAnimationLoop')

    root.addObject('RequiredPlugin', name='Sofa.Component.ODESolver.Backward')
    root.addObject('RequiredPlugin', name='Sofa.Component.LinearSolver.Direct')
    root.addObject('RequiredPlugin', name='Sofa.Component.Engine.Select')
    root.addObject('RequiredPlugin',
                   name='Sofa.Component.Constraint.Projective')
    root.addObject('RequiredPlugin',
                   name='Sofa.Component.SolidMechanics.FEM.Elastic')
    root.addObject('RequiredPlugin', name='Sofa.Component.Mass')

    root.addObject('EulerImplicitSolver',
                   rayleighStiffness="0.1", rayleighMass="0.1")
    linear_solver = root.addObject('SparseLDLSolver', name='linear_solver',
                                   applyPermutation="false", template=linear_solver_template)
    
    MO = root.addObject('MechanicalObject', name="DoFs")
    root.listOfMO.append(MO)
    root.addObject('UniformMass', name="mass", totalMass="320")
    root.addObject('RegularGridTopology', name="grid", nx="4", ny="4", nz="20",
                   xmin="-9", xmax="-6", ymin="0", ymax="3", zmin="0", zmax="19")
    root.addObject('BoxROI', name="box", box=[-10, -1, -0.0001, -5, 4, 0.0001])
    root.addObject('FixedConstraint', indices="@box.indices")
    root.addObject('HexahedronFEMForceField', name="FEM",
                   youngModulus="4000", poissonRatio="0.3", method="large")
    # root.addObject('GlobalSystemMatrixExporter',name = 'MatrixExporter')


    beam2 = root.addChild('dd')
    MO = beam2.addObject('MechanicalObject', name="DoFs2")
    
    root.listOfMO.append(MO)
    beam2.addObject('UniformMass', name="mass", totalMass="320")
    beam2.addObject('RegularGridTopology', name="grid", nx="4", ny="4", nz="20",
                 xmin="-9", xmax="-6", ymin="0", ymax="3", zmin="0", zmax="19")
    beam2.addObject('BoxROI', name="box", box=[-10, -1, -0.0001, -5, 4, 0.0001])
    beam2.addObject('FixedConstraint', indices="@box.indices")
    beam2.addObject('HexahedronFEMForceField', name="FEM",
                 youngModulus="4000", poissonRatio="0.3", method="large")

    Sofa.Simulation.init(root)
    #Sofa.Simulation.animate(root, 0.0001)

    return root


def PullingCable(attachedTo=None,
    name="Cable",
    cableGeometry=[[1.0, 0.0, 0.0],[0.0, 0.0, 0.0]],
    rotation=[0.0,0.0,0.0],
    translation=[0.0,0.0,0.0],
    uniformScale=1.0,
    pullPointLocation=None,
    initialValue=0.0,
    valueType="displacement"):
    """Creates and adds a cable constraint.

    The constraint apply to a parent mesh.

    Args:
        name (str): Name of the created cable.

        cableGeometry: (list vec3f): Location of the degree of freedom of the cable.

        pullPointLocation (vec3f): Position from where the cable is pulled. If not specified
        the point will be considered in the structure.

        valueType (str): either "force" or "displacement". Default is displacement.

        translation (vec3f):   Apply a 3D translation to the object.

        rotation (vec3f):   Apply a 3D rotation to the object in Euler angles.

        uniformScale (vec3f):   Apply an uniform scaling to the object.


    Structure:
        .. sourcecode:: qml

            Node : {
                    name : "Cable"
                    MechanicalObject,
                    CableConstraint,
                    BarycentricMapping
            }

    """
    #  This create a new node in the scene. This node is appended to the finger's node.
    cable = attachedTo.createChild(name)

    # This create a MechanicalObject, a componant holding the degree of freedom of our
    # mechanical modelling. In the case of a cable it is a set of positions specifying
    # the points where the cable is passing by.
    cable.createObject('MechanicalObject', position=cableGeometry,
                        rotation=rotation, translation=translation, scale=uniformScale)

    # Create a CableConstraint object with a name.
    # the indices are referring to the MechanicalObject's positions.
    # The last indice is where the pullPoint is connected.
    if pullPointLocation != None:
        cable.createObject('CableConstraint',
                            indices=range(len(cableGeometry)),
                            pullPoint=pullPointLocation,
                            value=initialValue,
                            valueType=valueType,
                            hasPullPoint=True
                            )
    else:
        cable.createObject('CableConstraint',
                            indices=range(len(cableGeometry)),
                            value=initialValue,
                            valueType=valueType,
                            hasPullPoint=False
                            )

    # This create a BarycentricMapping. A BarycentricMapping is a key element as it will create a bi-directional link
    # between the cable's DoFs and the parents's ones so that movements of the cable's DoFs will be mapped
    # to the finger and vice-versa;
    cable.createObject('BarycentricMapping', name="Mapping", mapForces=False, mapMasses=False)

    return cable