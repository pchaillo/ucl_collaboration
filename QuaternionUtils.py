# authors : Alexandre Kruszewski, Bruno Carrez, Paul Chaillou
# Date = 2023


from numpy import *

from splib3.numerics import quat as qt


# quelques fonctions bien utiles pour jouer avec les quaternions...
def getQuatFromPos(p):
    return array([p[3],p[4],p[5],p[6]])

def applyQuatToPos(q,p):
    p[3] = q[0]
    p[4] = q[1]
    p[5] = q[2]
    p[6] = q[3]
    # print(p)

def alignQuatX(q): # A METTRE DANS SPLIB ???? - From QuaternionUtils
    a = q.getEulerAngles()
    a[0]=0
    return qt.Quat.createFromEuler(a)

def fit_referential(axis, q): # a tester
    a = q.getEulerAngles()
    b[0] = a[axis[0]] 
    b[1] = a[axis[1]] 
    b[2] = a[axis[2]] 
    return qt.Quat.createFromEuler(b)


