"""
Auteur : Paul Chaillou
Contact : paul.chaillou@inria.fr
Année : 2023
Propriétaire : Université de Lille - CNRS 
License : Non définie, mais développé dans une démarche Open-Source et Logiciel Libre avec volonté de partage et de travail collaboratif. Développé dans un but non-marchand, en cas d'utilisation commerciale, merci de minimiser les prix et de favoriser le partage gratuit de tout ce qui peut l'être. A utiliser dans des buts prenant en compte les questions éthiques et morales (si possible non-militaire, ne rentrant pas dans le cadre de compétition, de monopole, ou de favorisation d'interets privés).

589531 - #A
589473 - #B
"""
import numpy as np
from phidget1002_0b.Phidget1002_0B import *
import time as time
import polhemus_liberty.python.PolhemusUSB as PolhemusUSB
import matplotlib.pyplot as plt

def channels(channel1,channel2,channel3,pres):
    channel3.apply_pressure(pres)
    channel2.apply_pressure(pres)
    channel1.apply_pressure(pres)


channel3 = PhidgetOutput(serial_nb = 589473,channel = 1)
channel2 = PhidgetOutput(serial_nb = 589473,channel = 2)
channel1 = PhidgetOutput(serial_nb = 589473,channel = 3)

p = PolhemusUSB.PolhemusUSB() # only once
positions = []
pos_z = []

pressionToApply = np.arange(0.0,1.6,0.2)

for i in pressionToApply:
    print(i)
    channels(channel1,channel2,channel3,i)
    time.sleep(1)
    p.UpdateSensors()
    positions.append(p.sensors[0].GetLastPosition())
    print('Position  : '+str(positions[-1]))
    pos_raw = p.sensors[0].GetLastPosition()
    pos_z.append(pos_raw[2])


try:
    input("Press Enter to Stop\n")
except (Exception, KeyboardInterrupt):
    pass

p.UpdateSensors()
# position = p.sensors[0].GetLastPosition()
print('Position  : '+str(positions))
print('With pression  : '+str(pressionToApply))

plt.plot(pressionToApply,pos_z,'ro')
plt.axis((0,2,-20,-24))
plt.show()

channel1.close_channel()
channel2.close_channel()
channel3.close_channel()