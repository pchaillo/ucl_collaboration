"""
Auteur : Paul Chaillou
Contact : paul.chaillou@inria.fr
Année : 2023
Propriétaire : Université de Lille - CNRS 
License : Non définie, mais développé dans une démarche Open-Source et Logiciel Libre avec volonté de partage et de travail collaboratif. Développé dans un but non-marchand, en cas d'utilisation commerciale, merci de minimiser les prix et de favoriser le partage gratuit de tout ce qui peut l'être. A utiliser dans des buts prenant en compte les questions éthiques et morales (si possible non-militaire, ne rentrant pas dans le cadre de compétition, de monopole, ou de favorisation d'interets privés).

589531 - #A
589473 - #B
"""

# from Phidget1002_0B import *

import phidget1002_0b.Phidget1002_0B as actuation
import polhemus_liberty.python.PolhemusUSB as tracking
import time as time
import Connexion_Function_ucl as connect

# p = tracking.PolhemusOvercoat() # only once
p = tracking.PolhemusOvercoat(init_pos = [110,0,0],axis = [2,-1,0], conversion_factor = 10)

nom_fichier = "s_d_act3_04_100"

nf, f = connect.OpenPrintFile2(file_type = ".csv",nom_fichier=nom_fichier,nom_dossier="workspace_record")

## PARAMETERS
# minimal_pressure = 0
# maximal_pressure = 160 # kPa
# step = 75

channel_1 = actuation.PhidgetOutput(serial_nb = 589531,channel = 3) # 589531

start = time.time()
current_time = 0
pression = 0
flag1 = 0
flag2 = 0

while current_time < 5 :
    current_time = time.time() - start


    if (current_time > 1 and flag1 == 0) :
        # print("LOULOULOU")
        flag1 = 1
        pression = 1

    if (current_time > 3 and flag2 == 0) :
        flag2 = 1
        pression = 0

    channel_1.apply_pressure(pression)

    # p.UpdateSensors()
    # position = p.sensors[0].GetLastPosition()
    # orientation = p.sensors[0].GetLastQuaternion()
    [position,orientation] = p.get_data()
    print('Position  : '+str(position)+ str(orientation)+str(pression) )
    f.write(str(position)+str(orientation)+str(pression)+'/'+str(current_time)+'\n')
    f.close()
    f = open(nf,'a')
    # print("test")

channel_1.close_channel()