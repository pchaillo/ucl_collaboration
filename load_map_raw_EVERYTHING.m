% Pour charger la carte (.map) avec le seuil
% avec les temps de tir enregistrés !

clc
clear all

close all

% path(path,'display_folder')
path(path,'record')
path(path,'matlab_code')

listeFichiersTxt = dir('./record/*') ;

for i = 1 : length(listeFichiersTxt)
%     i
    struct = listeFichiersTxt(i);
    name = struct.name;
    if length(name)>4
        if (name(1:4) == '2023') % si le dossier est celui attendu, alors on plot tout
            folder_name = strcat('record/',name);
            sub_list_files = dir(folder_name) ;

            for i = 1 : length(sub_list_files)
                struct = sub_list_files(i);
                name = struct.name;
%                 disp(name)
                if length(name)>4
                    if name(end-3:end) == '.map'
                        nom  = choix_chemin(folder_name,name);
                        disp(nom)
                        display_scan_map(nom,nom)
                    end
                end
            end

        end
    end
end


sub_list_files = dir('./record/2023-08-09 13:48:30/*') ;
