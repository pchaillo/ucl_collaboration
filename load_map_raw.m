% Pour charger la carte (.map) avec le seuil
% avec les temps de tir enregistrés !

clc
clear all

% close all

path(path,'display_folder')
path(path,'matlab_code')

nom = '01sponge.map';
% nom = '3D_acquisition_tracking_with_orientation.map';
% nom = '3D_acquisition_tracking_with_orientation_synchro.map';
% % nom = '3D_acquisition_tracking_with_orientation_synchro_matrices.map';

%% pour ne pas déplacer les fichiers
% path(path,'record') 
% folder_name = 'record/2023-08-08 16:21:20'
% nom  = choix_chemin(folder_name,nom)

titre = "Nuage de point, acquisition 3D vl6180x stiff flop"

%%
display_scan_map(nom,titre)