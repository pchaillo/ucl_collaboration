"""
Auteur : Paul Chaillou
Contact : paul.chaillou@inria.fr
Année : 2023
Propriétaire : Université de Lille - CNRS 
License : Non définie, mais développé dans une démarche Open-Source et Logiciel Libre avec volonté de partage et de travail collaboratif. Développé dans un but non-marchand, en cas d'utilisation commerciale, merci de minimiser les prix et de favoriser le partage gratuit de tout ce qui peut l'être. A utiliser dans des buts prenant en compte les questions éthiques et morales (si possible non-militaire, ne rentrant pas dans le cadre de compétition, de monopole, ou de favorisation d'interets privés).

589531 - #A
589473 - #B

"""

# import csv
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import proj3d
from math import sqrt

def dist3D(p1,p2):
        return sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2 + (p1[2] - p2[2]) ** 2)

def read_data(df_list):
    u = 0
    l = len(df_list)
    x = [0]*l
    y = [0]*l
    z = [0]*l
    dist = [0]*l
    for i in df_list : # version avec les positions, orientations et pressions 
        x[u] = float(i[0].replace("[",""))  # line.replace(char,"")
        y[u] = i[1]
        z_raw = i[2].replace("]","")
        # print(i[2])
        # print(z_raw)
        z_split = z_raw.split("[")
        z[u] = float(z_split[0])

        dist[u] = dist3D( p1=[110,0,0] , p2=[x[u],y[u],z[u]] )

        u = u + 1 
    return [x,y,z,dist,l]

data = pd.read_csv('./record/workspace_record/record_dynamic_no_zero.csv')
df_list = data.values.tolist()
[x_real,y_real,z_real,dist_real,l_real] = read_data(df_list)

print("real dynamic behavior :" + str(len(x_real)))

# data = pd.read_csv('./record/2023-12-07 18:58:27/FEM_Positions.csv') # record/2023-12-11 10:34:36
# data = pd.read_csv('./record/2023-12-11 10:34:36/FEM_Positions.csv') # record/2023-12-11 10:57:46
# data = pd.read_csv('./record/2023-12-11 10:57:46/FEM_Positions.csv') # record/2023-12-11 10:57:46
data = pd.read_csv('./record/2023-12-11 12:33:31/FEM_Positions.csv') # 2023-12-11 11:42:00 # 2023-12-11 11:35:12 #  2023-12-11 11:50:33 # 2023-12-11 12:02:03 # 2023-12-11 12:12:44
df_list = data.values.tolist()
[x,y,z,dist,l] = read_data(df_list)

print("Simulation dynamic behavior :" + str(len(x)))

# # Print 3D des positions
# fig = plt.figure(figsize=(8, 8))
# ax = fig.add_subplot(111, projection='3d')
# ax.axis('auto')
# ax.scatter(x, y, z)
# plt.show()

l = min(l,l_real) # on prend la liste la plus courte des 2 pour calculer les indices pour le plot

fact = 2.6 # 4 pour faire coller les courbes
z_real = [i*fact for i in z_real]

# Print 2D des distances
ind = range(0,l)
plt.title("Plotting 1-D array")
plt.xlabel("X axis")
plt.ylabel("Y axis")
# plt.plot(ind, dist, color="red", marker="o", label="Array elements")
# plt.plot(ind, x, color="red", marker="o", label="Array elements")
# plt.plot(ind, y, color="red", marker="o", label="Array elements")
plt.plot(ind, z[0:l], color="red", marker="o", label="Array elements")
plt.plot(ind, z_real[0:l], color="blue", marker="o", label="Array elements")

plt.legend()
plt.show()