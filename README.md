# ucl_stiff_flop

To download the repository and update submodules automatically :

```console
git clone git@gitlab.inria.fr:pchaillo/ucl_collaboration.git
cd ./ucl_collaboration/
git submodule update --init --recursive
```

# Recquirement :

Python libraries :
- Necessary :
```console
pip3 install spicy
```
- For Hardware Connexion
Libusb1 : For Polhemus Liberty
Pyserial : For Pressure Sensors
Phidget22 : For Pressure Actuation
```console
pip3 install pyserial
pip3 install libusb1
pip3 install Phidget22
```

- Necessary ? #TODO
Proxsuite : for Embedded function use => Remove ? #TODO
Transformations : For Imaging script
```console
pip3 install proxsuite
pip3 install transformations

```

Pour tout installer rapidement :
```console
pip3 install spicy
pip3 install pyserial
pip3 install libusb1
pip3 install Phidget22
pip3 install proxsuite
pip3 install transformations

```