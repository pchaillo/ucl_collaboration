#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Main example on how to use the state space extraction module
"""
import state_space_extraction as sse

# import coarse_beam
import Sofa

import numpy as np
import stiff_module as sm
from splib3.numerics import quat as qt

from matplotlib import pyplot as plt


PLOT = True
DT = 0.1

REDUCED_DOF_SIZE = 1  # each DOF = 2 states [velocity position] # correspond aux modes
DELTA_X = 10.0
DELTA_U = 10.0

NUM_OUTPUT = 1
NUM_INPUT = 1
STEP_INPUT_DURATION = 500
STEP_INPUT_AMPLITUDE = np.ones((NUM_INPUT, 1))*10.0
INPUT_EQ = np.zeros((NUM_INPUT, 1))


np.set_printoptions(threshold=10000, linewidth=1000, precision=5,suppress=True,floatmode='fixed')


def plot_signals(signals):
    """plots a list of signals

    Args:
        signals (_type_): _description_
    """
    n = len(signals)
    _, axes = plt.subplots(n, 1)
    for index in range(n):
        axes[index].plot(signals[index])
    plt.xlabel('steps')
    print('close the plot to continue')
    plt.show()


def main():
    """executed if launched from python
    """
    root = Sofa.Core.Node("root")
    my_scene = sm.createScene(root)
    Sofa.Simulation.init(my_scene)

    def R3_to_2V3(Rigid3): # passe de Rigid3, a 2 vec3 = 1 position et 1 oreintation
        # print(Rigid3)
        Position = list(Rigid3[0:3])
        Quaternion = qt.Quat(Rigid3[3:8])
        Euler = list(Quaternion.getEulerAngles())
        return Position + Euler

    def convert_2V3_to_R3(Vec3_tab): # passe de Rigid3, a 2 vec3 = 1 position et 1 oreintation
        Position = Vec3_tab[0:3]
        Quaternion = qt.Quat.createFromEuler(Vec3_tab[3:7])
        return list(np.append(Position, Quaternion))

    # Step 0: define the setters and the gettes
    def set_input(input_vector: np.ndarray):  # mandatory
        # for i in range(len(my_scene.actuator.forces)):
        #     for j in range(NUM_INPUT):
        my_scene.RigidFrames.stiff_flop1.Bellow11.SPC.value = [input_vector[0]]
        my_scene.RigidFrames.stiff_flop1.Bellow21.SPC.value = [input_vector[0]]
        my_scene.RigidFrames.stiff_flop1.Bellow31.SPC.value = [input_vector[0]]
        my_scene.RigidFrames.stiff_flop2.Bellow12.SPC.value = [input_vector[0]]
        my_scene.RigidFrames.stiff_flop2.Bellow22.SPC.value = [input_vector[0]]
        my_scene.RigidFrames.stiff_flop2.Bellow32.SPC.value = [input_vector[0]]

    def get_output() -> np.ndarray:  # mandatory
        return np.array([[my_scene.RigidFrames.DOFs.position[-1][0]]])

    def set_full_state(state):  # used by other functions
        my_scene.RigidFrames.DOFs.velocity.value = state[0]

        r3_state_tab = []
        for double_vec3 in state[1]:
            r3_state_tab.append(convert_2V3_to_R3(double_vec3))

        my_scene.RigidFrames.DOFs.position.value = r3_state_tab

    def get_full_state() -> np.array:  # mandatory
        # return np.array([my_scene.RigidFrames.DOFs.velocity.value.copy(), my_scene.RigidFrames.DOFs.position.value.copy()])
        all_velocities = my_scene.RigidFrames.DOFs.velocity.value.copy()
        all_positions = my_scene.RigidFrames.DOFs.position.value.copy()
        all_position_good_shape = []
        for r3 in all_positions:
            all_position_good_shape.append(R3_to_2V3(r3))
        # print(all_position_good_shape)
        return np.array([all_velocities, np.array(all_position_good_shape)])
            

    def step_input(step_index):  # mandatory
        input_vector = np.zeros((NUM_INPUT, 1))
        for i in range(NUM_INPUT):
            if step_index >= (i+1)*STEP_INPUT_DURATION/(NUM_INPUT+1):
                input_vector.flat[i] = STEP_INPUT_AMPLITUDE.flat[i]
        return np.array(input_vector)

    def save_sim_state():  # mandatory
        root.RigidFrames.DOFs.storeResetState()

    test_value = my_scene.RigidFrames.DOFs.position.value[10]
    value = R3_to_2V3(test_value)
    print(convert_2V3_to_R3(value))

    # Step 1: reach a steady state
    print('\nReaching steady state')

    def progress_print(p,e):
        print(f'progress = {100*p:>-3.0f}% max var ={e:>-4.2e}    ', end='\r')

    sse.reach_steady_state(
        root, get_full_state, dt=DT,
        precision=1e-6,
        progress_callback=progress_print)
    save_sim_state()

    # Step 2: generate data for the POD
    print('\nGenerate data for the POD = Simulate input responses')
    output_traj, state_traj, input_traj = sse.simulate_with_input_signal(
        root, set_input, get_output, get_full_state, step_input, DT, STEP_INPUT_DURATION,
        progress_callback=lambda p: print(f'progress = {100*p:>-3.0f}%', end='\r'))
    print("")
    if PLOT:
        plot_signals(
            [output_traj.reshape((STEP_INPUT_DURATION, -1))[:, -i-1] for i in range(NUM_OUTPUT)] +
            [input_traj.reshape((STEP_INPUT_DURATION, -1))[:, -i-1] for i in range(NUM_INPUT)])

    # Step 3: Go back to steady state and compute the POD arround it
    print('\nPerform POD')

    Sofa.Simulation.reset(root)
    equilibrium_state = get_full_state()
    equilibrium_output = get_output()

    full_transformation_position, singular_values = sse.proper_othorgnal_decomposition(
        state_traj[:, 1, :, :].reshape(STEP_INPUT_DURATION, -1),
        equilibrium_state[1])
    reduced_transformation_position = full_transformation_position[:,
                                                                   0:REDUCED_DOF_SIZE]

    global_transformation_matrix = sse.position_trans_to_global(
        reduced_transformation_position)

    # Step 4: define the new getters and setters around the equilibrium in the reduced state space
    print('\nCompute Matrices')

    def get_relative_output():
        return get_output().flatten() - equilibrium_output.flatten()

    def add_to_reduced_state_by_index(index, value):  # mandatory
        full_state = get_full_state()
        state = (global_transformation_matrix[:, index]*value).T + \
            equilibrium_state.flat
        full_state.flat = state
        set_full_state(full_state)

    def set_relative_input(input_vector: np.ndarray):  # mandatory
        # for i in range(len(my_scene.actuator.forces)):
        #     for j in range(NUM_INPUT):
        #         my_scene.actuator.forces[i,
        #                                  j] = input_vector.flat[j]+INPUT_EQ.flat[j]
        set_input(input_vector+10)


    def get_reduced_state():  # mandatory
        return ((get_full_state().flatten()-equilibrium_state.flatten())
                @ global_transformation_matrix)

    # Step 5: Compute the state space representation matrices around the equilibrium
    Sofa.Simulation.reset(root)
    print('\nCompute Matrices')
    state_matrix = sse.compute_state_matrix(
        root,
        add_to_reduced_state_by_index,
        get_reduced_state,
        save_sim_state,
        DELTA_X,
        progress_callback=lambda p: print(f'Mat A progress = {100*p:>-3.0f}%', end='\r'))

    input_matrix = sse.compute_input_matrix(
        root,
        set_relative_input,
        get_reduced_state,
        save_sim_state,
        NUM_INPUT,
        DELTA_U,
        progress_callback=lambda p: print(f'MatB progress = {100*p:>-3.0f}%', end='\r'))

    output_matrix = sse.compute_output_matrix(
        root,
        add_to_reduced_state_by_index,
        get_reduced_state,
        get_relative_output,
        save_sim_state,
        DELTA_X,
        progress_callback=lambda p: print(f'Mat C progress = {100*p:>-3.0f}%', end='\r'))

    feedthrough_matrix = sse.compute_feedthrough_matrix(
        root,
        set_relative_input,
        get_relative_output,
        save_sim_state,
        NUM_INPUT,
        DELTA_U,
        progress_callback=lambda p: print(f'Mat D progress = {100*p:>-3.0f}%', end='\r'))

    print('\n\ndone')

    print(f"A = \n{state_matrix}\n")
    print(f"B = \n{input_matrix}\n")
    print(f"C = \n{output_matrix}\n")
    print(f"D = \n{feedthrough_matrix}\n")

    print(f"max eig(A) = {max(np.abs(np.linalg.eig(state_matrix)[0])):>6.4f}")

    print('accuracy according to selected singular values'
          f'= {sum(singular_values[0:REDUCED_DOF_SIZE])/sum(singular_values):>-3.2f}')

    return root


# Function used only if this script is called from a python environment
if __name__ == '__main__':
    main()
