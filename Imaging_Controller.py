#!/usr/bin/env python;
# -*- coding: utf-8 -*-
import Sofa.Core
import Sofa.constants.Key as Key
from spicy import *
# from os import getcwd, chdir, mkdir
import os
from datetime import datetime
import csv
import time
import serial
import math as math
import transformations as tf
import numpy
import six
from scipy.spatial import distance
from math import dist, cos, sin
# from varname import nameof
# from sksurgerynditracker.nditracker import NDITracker # for Aurora tracking
# import polhemus_liberty.python.PolhemusUSB as PolhemusUSB # for Polhemus tracking

import time as time
from splib3.numerics import quat as qt

# # try :
import Connexion_Function_ucl as connect
# # except :
# #     import ucl_collaboration.Connexion_Function_ucl as connect

# from splib3.numerics import quat as qt

import vl6180x.VL6180X_Duino as SC


#FR :
# réunit toutes les fonctions python, permettant de :
# - réaliser des acquisitions 3D avec robot souple + télémètre

#EN:
# brings together all the python functions, allowing to:
# - acquire 3D shape with soft robot and depth sensor

class Synchro_variable():
    def __init__(self, synchro_flag): # un dictionnaire serait surement mieux ?
        self.synchro_flag = synchro_flag

        self.synchro_flag2 = synchro_flag

### - VIEWER - ###
class DirectSensorValue(Sofa.Core.Controller):
    """
    Pour print les positions dans le terminal - Poutre
    """
    def __init__(self,nom_dossier,nom_fichier,print_flag = False,beam_ind = "null",*args, **kwargs):
        Sofa.Core.Controller.__init__(self,args,kwargs)

        self.sensor = SC.SerialDuino()

        # nom_fichier = "data_record.map"
        self.nf, self.fichier = connect.OpenPrintFile2('.csv',nom_fichier,nom_dossier)

        self.time_ref = time.time()

        f = open(nom_fichier,'a')

        self.print_flag = print_flag

    def onAnimateBeginEvent(self,e):

        self.sensor.UpdateSensor() # récupération de la hauteur mesurée par le télémètre
        data = self.sensor.GetData()
        lux = data[0]
        dist = data[1]

        print(dist)



### - VIEWER - ###
class Topographic_Acquisition(Sofa.Core.Controller):
    """
    Pour print les positions dans le terminal - Poutre
    """
    def __init__(self,node,name,nom_dossier,nom_fichier,print_flag = False,beam_ind = "null",*args, **kwargs):
        Sofa.Core.Controller.__init__(self,args,kwargs)
        # self.node = args[0]
        # self.stiffNode=self.node.getChild('RigidFrames')
        self.stiffNode = node # for the generic one
        self.position = self.stiffNode.getObject(name)
        self.beam_ind = beam_ind

        self.sensor = SC.SerialDuino()

        # nom_fichier = "data_record.map"
        self.nf, self.fichier = connect.OpenPrintFile2('.map',nom_fichier,nom_dossier)

        self.time_ref = time.time()

        f = open(nom_fichier,'a')

        self.print_flag = print_flag

    def onAnimateBeginEvent(self,e):
        # pos = self.position.position.value[self.nb_poutre-1][0:3] # récupération de la position de l'effecteur (position de la dernière frame de la poutre centrale)
        # print('         ----       ')
        # print('Position effecteur : ',pos)
        # print('         ----       ')
        if self.beam_ind == "null":
            pos = numpy.array(self.position.position[0])
            # print(pos)
        else : 
            if self.beam_ind == math.ceil(self.beam_ind):
                pos = self.position.position.value[self.beam_ind][0:3]
            else : 
                pos = self.position.position.value[int(ceil(self.beam_ind))][0:3]


        self.sensor.UpdateSensor() # récupération de la hauteur mesurée par le télémètre
        data = self.sensor.GetData()
        lux = data[0]
        dist = data[1]

        time_txt = str(time.time() - self.time_ref) # récupération du temps

        x = pos[0] - dist
        y = pos[1]
        z = pos[2]


        self.fichier.write( str(x) + " " + str(y) + " " + str(z) + " "  + time_txt +'\n')

        if self.print_flag == True :
            print( str(x) + " " + str(y) + " " + str(z) + " "  + time_txt)

        self.fichier.close()
        # print("%%%% Positions Enregistrées en Csv %%%%")
        self.fichier = open(self.nf,'a')



### - VIEWER - ###
class Topographic_Acquisition_with_orientation(Sofa.Core.Controller):
    """
    Pour print les positions dans le terminal - Prend en compte l'orientation de l'effecteur
    """
    def __init__(self,node,name,nom_dossier,nom_fichier,print_flag = False,beam_ind = "null",*args, **kwargs):
        Sofa.Core.Controller.__init__(self,args,kwargs)
        # self.node = args[0]
        # self.stiffNode=self.node.getChild('RigidFrames')
        self.stiffNode = node # for the generic one
        self.position = self.stiffNode.getObject(name)
        self.beam_ind = beam_ind

        self.sensor = SC.SerialDuino()

        # nom_fichier = "data_record.map"
        self.nf, self.fichier = connect.OpenPrintFile2('.map',nom_fichier,nom_dossier)

        self.time_ref = time.time()

        f = open(nom_fichier,'a')

        self.print_flag = print_flag

    def onAnimateBeginEvent(self,e):
        # pos = self.position.position.value[self.nb_poutre-1][0:3] # récupération de la position de l'effecteur (position de la dernière frame de la poutre centrale)
        # print('         ----       ')
        # print('Position effecteur : ',pos)
        # print('         ----       ')
        if self.beam_ind == "null":
            pos = numpy.array(self.position.position[0]) # contains also orientation
            # print(pos)
        else : 
            if self.beam_ind == math.ceil(self.beam_ind):
                pos = self.position.position.value[self.beam_ind][0:3]
            else : 
                pos = self.position.position.value[int(ceil(self.beam_ind))][0:3]


        x_y_z = pos[0:3]
        orientation_quat = qt.Quat(pos[3:8])
        # print("Imaging_Controller.py test : ") # put the name of the file in the text to detect the zone in the terminal => make it easier to find back if we want to remove it
        # print(pos)
        # print(x_y_z)
        # print(pos[3:8])
        # print("==========")
        orientation = orientation_quat.getEulerAngles()

        self.sensor.UpdateSensor() # récupération de la hauteur mesurée par le télémètre
        data = self.sensor.GetData()
        lux = data[0]
        dist = data[1]

        time_txt = str(time.time() - self.time_ref) # récupération du temps

        x = pos[0] + dist*cos(orientation[1])*cos(orientation[2])
        y = pos[1] + dist*sin(orientation[2])
        z = pos[2] + dist*sin(orientation[1])


        self.fichier.write( str(x) + " " + str(y) + " " + str(z) + " "  + time_txt +'\n')

        if self.print_flag == True :
            print( str(x) + " " + str(y) + " " + str(z) + " "  + time_txt)

        self.fichier.close()
        # print("%%%% Positions Enregistrées en Csv %%%%")
        self.fichier = open(self.nf,'a')

### - VIEWER - ###
class Topographic_Acquisition_with_orientation_Synchro(Sofa.Core.Controller):
    """
    Pour print les positions dans le terminal - Prend en compte l'orientation de l'effecteur
    """
    def __init__(self,node,name,nom_dossier,nom_fichier,synchro_flag,print_flag = False,beam_ind = "null",*args, **kwargs):
        Sofa.Core.Controller.__init__(self,args,kwargs)
        # self.node = args[0]
        # self.stiffNode=self.node.getChild('RigidFrames')
        self.stiffNode = node # for the generic one
        self.position = self.stiffNode.getObject(name)
        self.beam_ind = beam_ind

        self.sensor = SC.SerialDuino()

        # nom_fichier = "data_record.map"
        self.nf, self.fichier = connect.OpenPrintFile2('.map',nom_fichier,nom_dossier)

        self.time_ref = time.time()

        f = open(nom_fichier,'a')

        self.print_flag = print_flag
        self.synchro_flag = synchro_flag
        # print(self.synchro_flag)

    def onAnimateBeginEvent(self,e):
        # print("Synchro_FlAG :")
        # print(self.synchro_flag)
        # print(self.synchro_flag.synchro_flag)
        if self.synchro_flag.synchro_flag == True :
            # pos = self.position.position.value[self.nb_poutre-1][0:3] # récupération de la position de l'effecteur (position de la dernière frame de la poutre centrale)
            # print('         ----       ')
            # print('Position effecteur : ',pos)
            # print('         ----       ')
            if self.beam_ind == "null":
                pos = numpy.array(self.position.position[0]) # contains also orientation
                # print(pos)
            else : 
                if self.beam_ind == math.ceil(self.beam_ind):
                    pos = self.position.position.value[self.beam_ind][0:3]
                else : 
                    pos = self.position.position.value[int(ceil(self.beam_ind))][0:3]


            x_y_z = pos[0:3]
            orientation_quat = qt.Quat(pos[3:8])
            # print("Imaging_Controller.py test : ") # put the name of the file in the text to detect the zone in the terminal => make it easier to find back if we want to remove it
            # print(pos)
            # print(x_y_z)
            # print(pos[3:8])
            # print("==========")
            orientation = orientation_quat.getEulerAngles()

            self.sensor.UpdateSensor() # récupération de la hauteur mesurée par le télémètre
            data = self.sensor.GetData()
            lux = data[0]
            dist = data[1]

            time_txt = str(time.time() - self.time_ref) # récupération du temps

            x = pos[0] + dist*cos(orientation[1])*cos(orientation[2])
            y = pos[1] + dist*sin(orientation[2])
            z = pos[2] + dist*sin(orientation[1])


            self.fichier.write( str(x) + " " + str(y) + " " + str(z) + " "  + time_txt +'\n')

            if self.print_flag == True :
                print( str(x) + " " + str(y) + " " + str(z) + " "  + time_txt)

            self.fichier.close()
            # print("%%%% Positions Enregistrées en Csv %%%%")
            self.fichier = open(self.nf,'a')

            self.synchro_flag.synchro_flag = False # on remet la variable a false en attendant qu'une autre fonction la remette à vrai


### - VIEWER - ###
class Topographic_Acquisition_with_orientation_matrices_Synchro(Sofa.Core.Controller):
    """
    Pour print les positions dans le terminal - Prend en compte l'orientation de l'effecteur
    """
    def __init__(self,node,name,nom_dossier,nom_fichier,synchro_flag,print_flag = False,beam_ind = "null",*args, **kwargs):
        Sofa.Core.Controller.__init__(self,args,kwargs)
        # self.node = args[0]
        # self.stiffNode=self.node.getChild('RigidFrames')
        self.stiffNode = node # for the generic one
        self.position = self.stiffNode.getObject(name)
        self.beam_ind = beam_ind

        self.sensor = SC.SerialDuino()

        # nom_fichier = "data_record.map"
        self.nf, self.fichier = connect.OpenPrintFile2('.map',nom_fichier,nom_dossier)

        self.time_ref = time.time()

        f = open(nom_fichier,'a')

        self.print_flag = print_flag
        self.synchro_flag = synchro_flag
        # print(self.synchro_flag)

        self.origin, self.xaxis, self.yaxis, self.zaxis = [0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]

    def onAnimateBeginEvent(self,e):
        # print("Synchro_FlAG :")
        # print(self.synchro_flag)
        # print(self.synchro_flag.synchro_flag)
        if self.synchro_flag.synchro_flag2 == True :
            # pos = self.position.position.value[self.nb_poutre-1][0:3] # récupération de la position de l'effecteur (position de la dernière frame de la poutre centrale)
            # print('         ----       ')
            # print('Position effecteur : ',pos)
            # print('         ----       ')
            if self.beam_ind == "null":
                pos = numpy.array(self.position.position[0]) # contains also orientation
                # print(pos)
            else : 
                if self.beam_ind == math.ceil(self.beam_ind):
                    pos = self.position.position.value[self.beam_ind][0:3]
                else : 
                    pos = self.position.position.value[int(ceil(self.beam_ind))][0:3]


            x_y_z = pos[0:3]
            orientation_quat = qt.Quat(pos[3:8])
            # print("Imaging_Controller.py test : ") # put the name of the file in the text to detect the zone in the terminal => make it easier to find back if we want to remove it
            # print(pos)
            # print(x_y_z)
            # print(pos[3:8])
            # print("==========")
            orientation = orientation_quat.getEulerAngles()

            self.sensor.UpdateSensor() # récupération de la hauteur mesurée par le télémètre
            data = self.sensor.GetData()
            lux = data[0]
            dist = data[1]

            time_txt = str(time.time() - self.time_ref) # récupération du temps

            x = pos[0] - dist
            y = pos[1]
            z = pos[2]

            Ry = tf.rotation_matrix(orientation[1], self.yaxis)
            Rz = tf.rotation_matrix(orientation[2], self.zaxis)

            pos_vect = [x, y, z,0]
            new_pos_vect = pos_vect * Ry * Rz

            x = new_pos_vect[0] 
            y = new_pos_vect[1]
            z = new_pos_vect[2]

            self.fichier.write( str(x) + " " + str(y) + " " + str(z) + " "  + time_txt +'\n')

            if self.print_flag == True :
                print( str(x) + " " + str(y) + " " + str(z) + " "  + time_txt)

            self.fichier.close()
            # print("%%%% Positions Enregistrées en Csv %%%%")
            self.fichier = open(self.nf,'a')

            self.synchro_flag.synchro_flag2 = False # on remet la variable a false en attendant qu'une autre fonction la remette à vrai