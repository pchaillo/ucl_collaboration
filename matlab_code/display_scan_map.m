function display_scan_map(nom,titre_choisi)

if nargin > 1 % Ce qui permet de mettre des variables par défaut sur Matlab
  titre = titre_choisi;
else
  titre = 'Titre par défaut';
end

out = load(nom);

si = size(out);
i = 0;

for u = 1 : si(1)
   % if  out(u,3) > -10
        i = i+1;
        carte_x(i) = out(i,1);
        carte_y(i) = out(i,2);
        carte_z(i) = out(i,3);
%         carte_time(i) = out(i,4);
   % end
end

if length(out) == 0 
    disp(" ")
    disp(" ATTENTION CARTE VIDE")
    disp("Carte vide veuillez suprimmer ces fichiers inutiles :") % mettre suppression automatique
    disp(nom)
    disp(" ATTENTION CARTE VIDE")
    disp(" ")
    return
end

if si(1) > 200

    figure()
    %surf(carte_x,carte_y,carte_z);
    % mesh(carte_x,carte_y,carte_z);
    pcshow([carte_z(:) carte_y(:) carte_x(:)])
    title([titre]);
    % colormap(autumn);
    axis equal
    grid off
    axis off
else
    disp("Carte non-affichée, moins de 200 points")
end

%% Deuxième affichage avec interpolation : potentiellement faux !
% 
% 
% % ptCloud = pointCloud(cat(3,carte_x,carte_y,carte_z)); % Recquire LIDAR ToolBox
% % ptCloudOut = pcmedian(ptCloud);
% 
% x = carte_z; % attention, should be carte_z !!!!
% y = carte_y;
% 
% v = lowpass(carte_x,0.9);
% v = carte_x;
% 
% [xq,yq] = meshgrid(min(x):1:max(x), min(y):1:max(y)); % on présuppose un pas de 1, ce qui n'est pas toujours juste ?
% vq = griddata(x,y,v,xq,yq,"cubic");
% figure()
% title([titre]);
% % vq = griddata(x,y,z,v,xq,yq,zq);
% %plot3(x,y,v,"ro")
% 
% disp("Data 1 :")
% disp([x;y;v])
% disp("Data 2 :")
% disp([xq;yq;vq])
% 
% hold on
% surf(xq,yq,vq)
% hold off