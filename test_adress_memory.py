#import ctypes
import ctypes
  
  
# variable declaration
val = True
  
# display variable
print("Actual value -", val)
  
# get the memory address of the python object 
# for variable list
x = id(val)
  
# display memory address
print("Memory address - ", x)
  
# get the value through memory address
a = ctypes.cast(x, ctypes.py_object).value
# display
print("Value - ", a)

ctypes.cast(x, ctypes.py_object).value = False

# get the value through memory address
a = ctypes.cast(x, ctypes.py_object).value
# display
print("Value - ", a)
print(ctypes.cast(x, ctypes.py_object).value)

memfield = (ctypes.c_char).from_address(x)
print(memfield)
memfield = False
print(memfield)
print(ctypes.cast(x, ctypes.py_object).value)
print((ctypes.c_char).from_address(x))
