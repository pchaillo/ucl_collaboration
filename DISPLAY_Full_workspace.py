"""
Auteur : Paul Chaillou
Contact : paul.chaillou@inria.fr
Année : 2023
Propriétaire : Université de Lille - CNRS 
License : Non définie, mais développé dans une démarche Open-Source et Logiciel Libre avec volonté de partage et de travail collaboratif. Développé dans un but non-marchand, en cas d'utilisation commerciale, merci de minimiser les prix et de favoriser le partage gratuit de tout ce qui peut l'être. A utiliser dans des buts prenant en compte les questions éthiques et morales (si possible non-militaire, ne rentrant pas dans le cadre de compétition, de monopole, ou de favorisation d'interets privés).

589531 - #A
589473 - #B

"""

# import csv
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import proj3d

data = pd.read_csv('./record/workspace_record/pos_orientation_06.csv')
# print(data)
df_list = data.values.tolist()

u = 0
l = len(df_list)
x = [0]*l
y = [0]*l
z = [0]*l

# for i in df_list : # version avec les positions seulement
#     x[u] = float(i[0].replace("[",""))  # line.replace(char,"")
#     y[u] = i[1]
#     z[u] = float(i[2].replace("]",""))
#     u = u + 1 

for i in df_list : # version avec les positions, orientations et pressions 
    x[u] = float(i[0].replace("[",""))  # line.replace(char,"")
    y[u] = i[1]
    z_raw = i[2].replace("]","")
    # print(i[2])
    # print(z_raw)
    z_split = z_raw.split("[")
    z[u] = float(z_split[0])
    u = u + 1 

# print(z)

print("test")


fig = plt.figure(figsize=(8, 8))
ax = fig.add_subplot(111, projection='3d')
ax.axis('auto')
ax.scatter(x, y, z)
# ax.scatter(data)

plt.show()