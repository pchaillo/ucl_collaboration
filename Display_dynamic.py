"""
Auteur : Paul Chaillou
Contact : paul.chaillou@inria.fr
Année : 2023
Propriétaire : Université de Lille - CNRS 
License : Non définie, mais développé dans une démarche Open-Source et Logiciel Libre avec volonté de partage et de travail collaboratif. Développé dans un but non-marchand, en cas d'utilisation commerciale, merci de minimiser les prix et de favoriser le partage gratuit de tout ce qui peut l'être. A utiliser dans des buts prenant en compte les questions éthiques et morales (si possible non-militaire, ne rentrant pas dans le cadre de compétition, de monopole, ou de favorisation d'interets privés).

589531 - #A
589473 - #B

"""

# import csv
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import proj3d
from math import sqrt
import numpy as np

def set_axes_equal(ax): # fonction récupérée sur internet
    """
    Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    """

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])


def dist3D(p1,p2):
        return sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2 + (p1[2] - p2[2]) ** 2)

data = pd.read_csv('./record/workspace_record/s_d_act2_04_100.csv')
# print(data)
df_list = data.values.tolist()

u = 0
l = len(df_list)
x = [0]*l
y = [0]*l
z = [0]*l
dist = [0]*l

# for i in df_list : # version avec les positions seulement
#     x[u] = float(i[0].replace("[",""))  # line.replace(char,"")
#     y[u] = i[1]
#     z[u] = float(i[2].replace("]",""))
#     u = u + 1 

for i in df_list : # version avec les positions, orientations et pressions 
    x[u] = float(i[0].replace("[",""))  # line.replace(char,"")
    y[u] = i[1]
    z_raw = i[2].replace("]","")
    # print(i[2])
    # print(z_raw)
    z_split = z_raw.split("[")
    z[u] = float(z_split[0])

    dist[u] = dist3D( p1=[110,0,0] , p2=[x[u],y[u],z[u]] )

    u = u + 1 

# Print 3D des positions
fig = plt.figure(figsize=(8, 8))
ax = fig.add_subplot(111, projection='3d')
ax.axis('auto') # auto
ax.set_box_aspect([1.0, 1.0, 1.0])
# ax.set_aspect('equal') # not working
ax.scatter(x, y, z)
set_axes_equal(ax)
plt.show()

# Print 2D des distances
ind = range(0,l)
plt.title("Plotting 1-D array")
plt.xlabel("X axis")
plt.ylabel("Y axis")
# plt.plot(ind, dist, color="red", marker="o", label="Array elements")
# plt.plot(ind, x, color="red", marker="o", label="Array elements")
# plt.plot(ind, y, color="red", marker="o", label="Array elements")
plt.plot(ind, z, color="red", marker="o", label="Array elements")

plt.legend()
plt.show()