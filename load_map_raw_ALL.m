% Pour charger la carte (.map) avec le seuil
% avec les temps de tir enregistrés !

clc
clear all

% close all

% path(path,'display_folder')
path(path,'record') 

nom_carte = '3D_acquisition_tracking.map';
% nom = '3D_acquisition_tracking_with_orientation.map';
% nom = '3D_acquisition_tracking_with_orientation_synchro.map';
% % nom = '3D_acquisition_tracking_with_orientation_synchro_matrices.map';


listeFichiersTxt = dir('./record/*') ;

for i = 1 : length(listeFichiersTxt)
    i
    struct = listeFichiersTxt(i);
    name = struct.name;
    if length(name)>4
        if (name(1:4) == '2023')
            folder_name = strcat('record/',name);
            nom  = choix_chemin(folder_name,nom_carte);
            display_scan_map(nom,name)
        end
    end
end

